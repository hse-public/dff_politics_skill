import re
import json
import logging

from df_engine.core import Actor, Context

logger = logging.getLogger(__name__)

def ask_for_restart(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"^start.+beginning|\brestart\b", request, re.IGNORECASE) != None

def ask_terminologies(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+terminolog(y|ies)", request, re.IGNORECASE) != None

def ask_politicians(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+politician", request, re.IGNORECASE) != None

def ask_countries(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+(countr(y|ies)|nation)", request, re.IGNORECASE) != None

def ask_wars(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+war", request, re.IGNORECASE) != None

# --------- List of Terminologies ---------

def ask_politics(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+politic", request, re.IGNORECASE) != None

def ask_democracy(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+democrac(y|ies)", request, re.IGNORECASE) != None

def ask_monarchy(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+monarch(y|ies)", request, re.IGNORECASE) != None

def ask_republics(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+republic", request, re.IGNORECASE) != None

# --------- List of Wars ---------

def ask_ww1(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+(first world war|world war 1|WW ?1)", request, re.IGNORECASE) != None

def ask_ww2(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+(second world war|world war 2|WW ?2)", request, re.IGNORECASE) != None

def ask_cold_war(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+cold war", request, re.IGNORECASE) != None


# --------- List of Politicians ---------

def ask_putin(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+\bputin\b", request, re.IGNORECASE) != None

def ask_xi(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+\bxi\b", request, re.IGNORECASE) != None


# --------- List of Countries ------------

def ask_china(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+china", request, re.IGNORECASE) != None

def ask_egypt(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+egypt", request, re.IGNORECASE) != None

def ask_france(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+france", request, re.IGNORECASE) != None

def ask_germany(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+germany", request, re.IGNORECASE) != None

def ask_india(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+india", request, re.IGNORECASE) != None

def ask_indonesia(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+indonesia", request, re.IGNORECASE) != None

def ask_iran(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+iran", request, re.IGNORECASE) != None

def ask_libya(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+libya", request, re.IGNORECASE) != None

def ask_syria(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+syria", request, re.IGNORECASE) != None

def ask_russia(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+russia", request, re.IGNORECASE) != None

def ask_usa(ctx: Context, actor: Actor) -> bool:
    request = ctx.last_request.strip()
    return re.search(r"tell.+(usa|america|state|U.S.)", request, re.IGNORECASE) != None