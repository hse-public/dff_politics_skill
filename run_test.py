import logging
import time

from scenario.main import actor

from run_interactive import turn_handler

from scenario.data.putin import putin_testing_dialog
from scenario.data.xi_jinping import xi_testing_dialog
from scenario.data.indonesia import indonesia_testing_dialog
from scenario.data.china import china_testing_dialog
from scenario.data.egypt import egypt_testing_dialog
from scenario.data.india import india_testing_dialog
from scenario.data.iran import iran_testing_dialog
from scenario.data.libya import libya_testing_dialog
from scenario.data.russia import russia_testing_dialog
from scenario.data.syria import syria_testing_dialog
from scenario.data.usa import usa_testing_dialog
from scenario.data.germany import germany_testing_dialog
from scenario.data.france import france_testing_dialog
from scenario.data.world_war_1 import world_war_1_testing_dialog
from scenario.data.world_war_2 import world_war_2_testing_dialog
from scenario.data.cold_war import cold_war_testing_dialog
from scenario.data.democracy import democracy_testing_dialog
from scenario.data.monarchy import monarchy_testing_dialog
from scenario.data.republics import republics_testing_dialog
from scenario.data.politics import politics_testing_dialog

logger = logging.getLogger(__name__)


# testing
testing_dialog = monarchy_testing_dialog

def run_test():
    ctx = {}
    for in_request, true_out_response in testing_dialog:
        out_response, ctx = turn_handler(in_request, ctx, actor, true_out_response=true_out_response)
        print(f"{in_request=} -> {out_response}")

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s-%(name)15s:%(lineno)3s:%(funcName)20s():%(levelname)s - %(message)s", level=logging.INFO
    )
    run_test()