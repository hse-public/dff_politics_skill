import re
from typing import Any, Tuple, Callable

from df_engine.core import Context, Actor

from scenario.transformers import * 

# some constants
idontknow = "Sorry, I don't know about that."

def topic_response(QnA_func: Callable, request: str):
    try: 
        ## why this got initialized with 'text' at the start??
        if request == 'text':
            return 
        answer = QnA_func(request)
        return answer
    except: 
        return idontknow 


# Dialog flow 
# --- terminologies ---
def politics_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Politics, ctx.last_request)

def democracy_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Democracy, ctx.last_request)

def monarchy_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Monarchy, ctx.last_request)

def republics_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Republics, ctx.last_request)


# --- wars ----
def ww1_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_WW1, ctx.last_request)

def ww2_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_WW2, ctx.last_request)

def cold_war_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_ColdWar, ctx.last_request)

# --- politicians ----
def putin_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Putin, ctx.last_request)

def xi_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Xi, ctx.last_request)

# --- countries ---
def china_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_China, ctx.last_request)

def egypt_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Egypt, ctx.last_request)

def france_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_France, ctx.last_request)

def germany_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Germany, ctx.last_request)

def india_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_India, ctx.last_request)

def indonesia_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Indonesia, ctx.last_request)

def iran_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Iran, ctx.last_request)

def libya_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Libya, ctx.last_request)

def russia_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Russia, ctx.last_request)

def syria_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_Syria, ctx.last_request)

def usa_topic_response(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return topic_response(QnA_USA, ctx.last_request)

# ---- intros ----
def bot_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return '''Welcome to Politics Chats. 
You could ask about terminologies, wars, countries or politicians.
Example: 
Tell me about wars

Or start from beginning: 
Please restart
Start from beginning
'''

def countries_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''

def terminologies_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return '''Welcome to terminologies subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning: 
Please restart
Start from beginning
'''

def wars_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return '''Welcome to wars subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology

Or move subtopic to specific wars (world war 1, world war 2, cold war):
Tell me about cold war

Or start from beginning: 
Please restart
Start from beginning
'''

def politicians_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return '''Welcome to politicians subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology
Tell me about wars

Or move subtopic to specific politician (Putin, Xi Jinping):
Tell me about Putin

Or start from beginning: 
Please restart
Start from beginning
'''


# ----- countries -------

country_menu = '''
You could jump to subtopics:
Tell me about terminologies
Tell me about wars

Or go to other country:
Tell me about Russia

Or start from beginning: 
Please restart
Start from beginning
'''

def china_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on China.
{country_menu}
'''

def egypt_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Egypt.
{country_menu}
'''

def france_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on France.
{country_menu}
'''

def germany_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Germany.
{country_menu}
'''

def india_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on India.
{country_menu}
'''

def indonesia_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Indonesia.
{country_menu}
'''

def iran_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Iran.
{country_menu}
'''

def libya_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Libya.
{country_menu}
'''

def russia_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Russia.
{country_menu}
'''

def syria_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on Syria.
{country_menu}
'''

def usa_intro(ctx: Context, actor: Actor, *args, **kwargs) -> Any:
    return f'''Welcome to topic on USA.
{country_menu}
'''
