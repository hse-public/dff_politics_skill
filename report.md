# Final report 

1. We strip out the plan to deploy to cloud and create fancy UI. Instead we have simple UI
with embedded html and CSS to make it visually pleasant. 
2. We create docker image of the app, available as image archive in Google drive. Due to slow internet connection we were not able to deploy the 7.2 Gb image to Docker Hub without encountering timeout. 
3. We have hierarchical sub-topics with 4 main topics: `Terminology`, `War`, `Nations`, and `Politicians`
4. The Politics chatbots has 2 interface, website or command line. 

# Historical report
# Week 1 

## TODO: 
* need to add more QnA scenarios, like high level Monarchy, Democracy, China, USA, Indonesia
* Create high level dialog flow scenario so we don't go to wide on some subject and to narrow on others. 
* call translation API for Indonesia language QnA
    -  there exist BERT QnA for Indonesia but light version not large like we currently use for English.
    -  We need to test it and figure out if that is useful or using translation API is better.
* Currently initial start up and getting response from transformers is slow. Can we improve the speed?

## WIP:
* QnA for Indonesian President. 
* Research on how use Google Translation API.

## Done:
* Implementation for HuggingFace Question/Answer code for English model.
    - created QnA for Putin and Xi Jinping in English    

### Integration test output
```
(dff_politics_skill) wismineffendi@Wismins-Mac-mini dff_politics_skill % python example_transformers.py
2021-12-30 14:46:30,244-           root: 99:        turn_handler():INFO - in_request=who is russian president -> vladimir vladimirovich putin
2021-12-30 14:46:38,176-           root: 99:        turn_handler():INFO - in_request=who is Putin -> vladimir vladimirovich putin , born 7 october 1952 , is a russian politician and former intelligence officer who is serving as the current president of russia
2021-12-30 14:46:46,154-           root: 99:        turn_handler():INFO - in_request=when did Putin born -> 7 october 1952
2021-12-30 14:46:54,004-           root: 99:        turn_handler():INFO - in_request=is Putin KGB agent -> putin worked as a kgb foreign intelligence officer for 16 years , rising to the rank of lieutenant colonel
2021-12-30 14:47:01,846-           root: 99:        turn_handler():INFO - in_request=does Putin support the church -> putin has collaborated closely with the russian orthodox church
2021-12-30 14:47:09,683-           root: 99:        turn_handler():INFO - in_request=does russian church support Putin -> putin has collaborated closely with the russian orthodox church . patriarch kirill of moscow , head of the church , endorsed his election in 2012 stating putin ' s terms were like " a miracle of god . "
2021-12-30 14:47:17,495-           root: 99:        turn_handler():INFO - in_request=who lead russian orthodox church -> patriarch kirill of moscow
2021-12-30 14:47:25,268-           root: 99:        turn_handler():INFO - in_request=what Kirill say about Putin -> putin ' s terms were like " a miracle of god . "
2021-12-30 14:47:33,092-           root: 99:        turn_handler():INFO - in_request=did putin wins election -> putin received 76 % of the vote in the 2018 election and was re - elected for a six - year term ending in 2024 .
2021-12-30 14:47:40,910-           root: 99:        turn_handler():INFO - in_request=does Putin support LGBT -> putin has promoted explicitly conservative policies
2021-12-30 14:47:48,692-           root: 99:        turn_handler():INFO - in_request=is russia prosperous under putin -> russian economy grew for eight straight years
2021-12-30 14:47:56,562-           root: 99:        turn_handler():INFO - in_request=what did Putin do internationally -> putin led russia to victory in the second chechen war and the russo - georgian war
2021-12-30 14:48:05,204-           root: 99:        turn_handler():INFO - in_request=who is president of China -> xi jinping
2021-12-30 14:48:13,832-           root: 99:        turn_handler():INFO - in_request=how is Xi foreign policy -> projecting a more nationalistic and assertive china on the world stage
2021-12-30 14:48:22,412-           root: 99:        turn_handler():INFO - in_request=did Xi try to join CPC -> xi applied to join the chinese communist party ten times and was finally accepted on his tenth attempt in 1974
2021-12-30 14:48:31,008-           root: 99:        turn_handler():INFO - in_request=when Xi become president of china -> 2013
2021-12-30 14:48:39,600-           root: 99:        turn_handler():INFO - in_request=when did Xi born -> 15 june 1953
2021-12-30 14:48:48,231-           root: 99:        turn_handler():INFO - in_request=what is Xi climate policy -> china will " strengthen its 2030 climate target ( ndc ) , peak emissions before 2030 and aim to achieve carbon neutrality before 2060
2021-12-30 14:48:56,843-           root: 99:        turn_handler():INFO - in_request=what is Xi project -> the belt and road initiative
2021-12-30 14:49:05,487-           root: 99:        turn_handler():INFO - in_request=who involve in BRI -> numerous infrastructure development and investment projects throughout asia , europe , africa , and the americas
2021-12-30 14:49:14,111-           root: 99:        turn_handler():INFO - in_request=what is belt and road initiative -> the belt and road initiative ( bri ) has been called xi ' s " signature project " since 2013 , involving numerous infrastructure development and investment projects throughout asia , europe , africa , and the americas
2021-12-30 14:49:22,752-           root: 99:        turn_handler():INFO - in_request=what Xi fight for -> crack down on corruption
2021-12-30 14:49:31,407-           root: 99:        turn_handler():INFO - in_request=why Xi crack down on corruption -> fighting corruption was one of the toughest challenges for the party
2021-12-30 14:49:40,107-           root: 99:        turn_handler():INFO - in_request=what Xi said in interview -> during an interview in 2000 , xi said , " people who have little contact with power , who are far from it , always see these things as mysterious and novel . but what i see is not just the superficial things : the power , the flowers , the glory , the applause . i see the bullpens and how people can blow hot and cold . i understand politics on a deeper level . "
2021-12-30 14:49:48,804-           root: 99:        turn_handler():INFO - in_request=was Xi misfortune -> the misfortunes and suffering of his family in his early years
```

# Week 2 

## TODO: 
    - Add more general subtopics like World war 1, World war 2, Arab Spring, 
    Korean war, Vietnam war and additional countries like Japan, Britain, Singapore, etc.
    - Currently we rely only on the Huggingface Bert to do all the heavy work, we only use 
    the DF engine with simple regex split, all in one node. 

## Issue encouters: 
    - We observed that removing '?' at the end of the query would resulted in more verbose answer, but Indonesian translation by API always has '?' on it's English counterpart. Should we manually remove this before send the query to Huggingface BERT QnA? Something to ponder the plus and minus.
    - Sometime the Indonesia query could not produce the results as good as the English one, especially we saw this while working on subtopic of 'Cold war'.  
    - We observe that the United States could sometimes in the form of USA, or U.S. or United States and so far we haven't found way to unite them. For example if the wiki_text use 'USA' and we query with 'U.S.' (as translation of Amerika Serikat in Indonesian), we won't get the proper answer. 
     
### Done: 
    - Support for second language (Indonesia) via translation service with 
      automatic language detection. 
    - Support option of translation using Azure translate and Google translate API. 
    - Refactor implementation from week 1.
    - Currently we support the sub-topics of Cold war and current political affairs for the following countries: Egypt, France, Germany, India, Indonesia, Iran, Libya, Russia, Syria, USA, and China. 
### Integration test output (both in English and Indonesian)

(see the output in the README.md)

# Week 3 

This week progress is a bit slower. We will catch up in the next couple days to 
accomplish more before my scheduled meeting on Monday 17 Jan. 

TODO: 
(1) Create Front end webside in Vue JS and deploy the whole App (backend and frontend)
on the cloud. 
(2) Provide follow up suggestion of related entities they could ask, especially when
the orginal question resulted in no answer. 
(3) Reorganize the information in more hierachical structure so that it would be easier to expands in the future. 

WIP:
(1) Add integration test for subtopic politics.  


DONE:
(1) Remove the extra '?' when original question in Bahasa don't have question mark.
(2) Retest questions related to entity linking for words like USA, America, etc.
(3) Add FastAPI interface for API call.  
(4) Add data for Politics in general. (subtopic Politics)


# Week 3.5 (for meeting on 01/??/22)

TODO: 
(1) Create Vue3 JS front end.
(2) Figure out how to dockerize the project (backend). 
(3) More testing and fine tuning the wiki text to reduce garbage answers.
(4) Deploy both backend and frontend to cloud. 

WIP:
(1) Learning Vue3, how to use it for our simple app.

Issues encounter:
- None

DONE: 
(1) Reorganization of the political chats into 4 main topics (terminology, countries, wars, and politicians)
(2) Update the wiki data and test data to comply with new reorganization of chats.
(3) Websocket works for hierarchical subtopics. e.g. Global -> Nations -> China 