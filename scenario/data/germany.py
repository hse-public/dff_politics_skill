germany_wiki_text = """
Germany is a federal, parliamentary, representative democratic republic. Federal legislative power is vested in the parliament consisting of the Bundestag (Federal Diet) and Bundesrat (Federal Council), which together form the legislative body. 
The Bundestag is elected through direct elections using the mixed-member proportional representation system. 
The members of the Bundesrat represent and are appointed by the governments of the sixteen federated states. 
The German political system operates under a framework laid out in the 1949 constitution known as the Grundgesetz (Basic Law).

The chancellor of Germany, officially the Federal Chancellor of the Federal Republic of Germany (German: Bundeskanzler(in) der Bundesrepublik Deutschland), 
is the head of the federal government of Germany and the commander in chief of the German Armed Forces during wartime (peacetime command goes to the Federal Minister of Defence). 
The chancellor is the chief executive of the Federal Cabinet and heads the executive branch.

Angela Dorothea Merkel (born 17 July 1954) is a German politician and scientist who served as the chancellor of Germany from 2005 to 2021. 
A member of the Christian Democratic Union (CDU), she previously served as leader of the Opposition from 2002 to 2005 and as Leader of the CDU from 2000 to 2018. 
Merkel was the first female chancellor of Germany.

Olaf Scholz (born 14 June 1958) is a German politician serving as chancellor of Germany since 8 December 2021. 
A member of the Social Democratic Party (SPD), Olaf Scholz previously served as Vice Chancellor of Germany under Angela Merkel and as Federal Minister of Finance from 2018 to 2021.
"""

germany_testing_dialog = [
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ("Tell me about Germany", 'germany is a federal , parliamentary , representative democratic republic'),
    ('Who is the leader of Germany?', 'the chancellor of germany'),
    ('What is chancellor in Germany?', 'federal chancellor of the federal republic of germany'),
    ('Who is the first female chancellor of Germany?', 'angela dorothea merkel'),
    ('How long Merkel serve as chancellor?', '2005 to 2021'),
    ('What kind of democratic country is Germany?', 'representative democratic republic'),
    ('How many states in Germany?', 'sixteen'),
    ('What party is Merkel?', 'christian democratic union'),
    ('Who is the current chancellor of Germany?', 'olaf scholz'),
    ('When Olaf Scholz become cancellor?', '8 december 2021'),
    ('Which party Olaf Scholz came from?', 'social democratic party'),
    ('What Merkel did before becoming chancellor?', 'leader of the opposition'),
    ('When Merkel was leader of the opposition?', '2002 to 2005'),
    ('What Olaf Scholz did before becoming chancellor?', 'vice chancellor of germany under angela merkel and as federal minister of finance'),
    # dalam Bahasa Indonesia
    ('Siapa pemimpin di Jerman?', 'Kanselir Jerman'),
    ('Apa itu Kanselir Jerman?', 'Kanselir federal Republik Federal Jerman'),
    ('Siapa wanita pertama menjadi kanselir Jerman ', 'angela dorothea merkel'),
    ('Berapa lama Merkel menjadi kanselir Jerman?', '2005 hingga 2021'),
    ('Demokrasi seperti apa Jerman itu?', 'Republik demokratik perwakilan'),
    ('Ada berapa negara bagian di Jerman?', 'Enam belas'),
    ('Merkel dari partai apa?', 'Persatuan demokrasi Kristen'),
    ('Siapa kanselir Jerman sekarang?', 'olaf scholz'),
    ('Kapan Olaf Scholz menjadi kanselir Jerman?', '8 Desember 2021'),
    ('Olaf Scholz dari partai apa?', 'Pesta sosial demokrasi'),
    ('Apa yang Merkel lakukan sebelum menjadi kanselir Jerman?', 'Pemimpin oposisi'),
    ('Kapan Merkel menjadi pemimpin oposisi?', '2002'),
    ('Apa yang Olaf Scholz lakukan sebelum menjadi kanselir Jerman?', 'Wakil Kanselir Jerman di Bawah Angela Merkel dan Sebagai Menteri Keuangan Federal'),
]