france_wiki_text = """
France is representative democracy organised as a unitary, semi-presidential republic. 
As one of the earliest republics of the modern world, democratic traditions and values are deeply rooted in French culture, 
identity and politics. The Constitution of the Fifth Republic was approved by referendum on 28 September 1958, 
establishing a framework consisting of executive, legislative and judicial branches. 
It sought to address the instability of the Third and Fourth Republics by combining elements of both parliamentary and presidential systems, 
whilst greatly strengthening the authority of the executive relative to the legislature.

The executive branch has two leaders, the President of the Republic and the Prime Minister. The President of the Republic, currently Emmanuel Macron, is the head of state, elected directly by universal adult suffrage for a five-year term. 
The Prime Minister, currently Jean Castex, is the head of government, appointed by the President of the Republic to lead the Government of France. The President has the power to dissolve Parliament or circumvent it by submitting referendums directly to the people; the President also appoints judges and civil servants, negotiates and ratifies international agreements, as well as serves as commander-in-chief of the Armed Forces. 
The Prime Minister determines public policy and oversees the civil service, with an emphasis on domestic matter.

Emmanuel Jean-Michel Frédéric Macron (born 21 December 1977) is a French politician who has been serving as the president of France since 14 May 2017.

At the age of 39, Macron became the youngest president in French history. 
Macron appointed Édouard Philippe as prime minister, and in the 2017 French legislative election a month later Macron's party, renamed La République En Marche (LREM), secured a majority in the National Assembly. 
During Macro presidency, Macron has overseen several reforms to labour laws and taxation. 
Opposition to Macron reforms, particularly a proposed fuel tax, culminated in the 2018 yellow vests protests and other protests. 
In 2020, Macron appointed Jean Castex as prime minister following Philippe's resignation.
"""

france_testing_dialog = [
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about France', 'france is representative democracy organised as a unitary , semi - presidential republic .'),
    ('Who is leader of France?', 'president of the republic'),
    ('What kind of democracy is France?', 'representative'),
    ('What are government branches in France?', 'executive , legislative and judicial branches'),
    ('Who leads executive branch in France?', 'the president of the republic and the prime minister'),
    ('What is the power of president of France?', 'dissolve parliament or circumvent it by submitting referendums directly to the people'),
    ('What is the power of prime minister of France?', 'appointed by the president of the republic to lead the government of france'),
    ('What republic is France?', 'unitary , semi - presidential republic'),
    ('How many republic exist in France?', 'fifth'),
    ('Was there referendum in France?', 'the constitution of the fifth republic was approved by referendum on 28 september 1958'),
    ('Why there was referendum in France?', 'to address the instability of the third and fourth republics'),
    ('What is the power of prime minister in France?', 'appointed by the president of the republic to lead the government of france'),
    ('Who is current president of France?', 'emmanuel macron'),
    ('What did Emmanuel Macron do?', 'reforms to labour laws and taxation'),
    ('Did Macron has opposition?', 'opposition to macron reforms , particularly a proposed fuel tax , culminated in the 2018 yellow vests protests and other protests'),
    ('Who is current prime minister of France?', 'jean castex'),
    ('Who is prime minister before Jean Castex?', 'edouard philippe'),
    ('When was Edouard Philippe prime minister in France?', '2017'),
    ('When Jean Castex become prime minister?', '2020'),
    ('When Macron become president?', '14 may 2017'),
    ('Who is appointed prime minister by Macron', 'jean castex'),
    # dalam Bahasa Indonesia
    ('siapa pemimpin negara Prancis?', 'Presiden Republik'),
    ('Apa bentuk demokrasi di Prancis?', 'demokrasi perwakilan'),
    ('Ada berapa cabang pemerintahan di Prancis?', 'cabang eksekutif, legislatif dan yudikatif'),
    ('Siapa memimpin cabang eksekutif di Prancis?', 'Presiden Republik dan Perdana Menteri'),
    ('Apa kekuasaan presiden di Prancis?', 'Presiden memiliki kekuatan untuk membubarkan parlemen atau menghindarinya dengan mengajukan referendum langsung kepada rakyat.'),
    ('Apa kekuasaan perdana menteri di Prancis?', 'Ditunjuk oleh presiden republik untuk memimpin pemerintahan Perancis'),
    ('Republik apa Prancis itu sekarang?', 'kesatuan , semi - republik presiden'),
    ('Kenapa ada referendum di Prancis?', 'Untuk mengatasi ketidakstabilan republik ketiga dan keempat'),
    ('Republik apa di Prancis sekarang?', 'Republik kelima'),
    ('Apakah ada referendum di Prancis?', 'Konstitusi republik kelima disetujui oleh referendum pada tanggal 28 September 1958.'),
    ('Siapa presiden Prancis sekarang?', 'emmanuel macron'),
    ('Apakah Macron mengalami perlawanan?', 'Oposisi terhadap reformasi Macron, terutama pajak bahan bakar yang diusulkan, memuncak dalam protes rompi kuning 2018.'),
    ('Siapa perdana menteri Prancis sekarang?', 'jean castex'),
    ('Siapa perdana menteri sebelum Jean Castex?', 'edouard philippe'),
    ('Kapan Edouard Philippe menjadi perdana menteri?', '2017'),
    ('Kapan Jean Castex menjadi perdana menteri?', '2020'),
    ('Kapan Macron menjadi presiden?', '14 Mei 2017'),
    ('Siapa yang diangkat Macron menjadi perdana menteri?', 'edouard philippe'),
    ('Siapa diangkat Macron menjadi perdana menteri terbaru?', 'jean castex'),
]