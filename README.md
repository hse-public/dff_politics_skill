# dff-politics-skill

## Description

Bilingual chatbot politics skills in English and Indonesian.

We utilize the pre-learned models from Huggingface for QnA of subtopics. 
We call external translation API (support Azure and Google) to support the Indonesian language.

  Currently we support the topics on `Terminology`, `Countries`, `Wars`, and `Politicians`.
  - Terminology has sub-topics: Politics, Democracy, Monarchy and Republic
  - Wars has sub-topics of World War 1, World War 2 and Cold war 
  - Countries has sub-topics for following countries: Egypt, France, Germany, India, Indonesia, Iran, Libya, Russia, Syria, USA, and China. 
  - Policitians has sub-topics for Putin and Xi Jinping


## Installation

### 1. Docker 

The docker image is about 7.2 Gb, we have the image archived in the following google drive link: 
  [wismin.dff-politics-skill](https://drive.google.com/file/d/1--F4McnAH7ViObNb3d5aWLgc-uRopY7s/view?usp=sharing)

We attempted to upload to Docker Hub, but due to our slow internet connection, it kept timing out, thus we upload the image archive to Google drive instead. 

After download the image archive from link above:
1. Load the archive into local docker image: 

```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab>docker load -i wismin.dff-politics-skill.image
Loaded image: wismin/dff-politics-skill:latest
```

2. Check docker images: 

```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab>docker images 
REPOSITORY                  TAG       IMAGE ID       CREATED        SIZE
wismin/dff-politics-skill   latest    090f29218e69   13 hours ago   7.14GB
```

3. You will need AZURE translate key to run the container properly. 
(please PM/email me for azure key if you don't have one yet). 
You need to replace the key below with valid key. (the key below is fake key)

```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab>docker run -p 8000:8000 -e AZURE_TRANSLATE_KEY=9123456f6ec34b67a3bd666ddb9022a6 wismin/dff-politics-skill 
```

4. Then open browser and navigate to: `localhost:8000 `

5. Start chatting in politics, with main topics: Terminologies, Wars, Countries, Politicians and navigate to subtopics in each main topics for question and answer. 

![image info](./images/chatbots_ww1_ww2.png)

### 2. Local installation

Prerequisite: 
1. Python virtual environment with Python 3.8
   e.g. we have created virtual env with name: `dff_politics_skill`
2. Git command line 
3. Environment variable `azure_translate_key` set with valid api key (Windows)
   Or `AZURE_TRANSLATE_KEY` for Linux or MacOS. 

Steps:
1. Clone GitLab repo. 
```
  (dff_politics_skill) C:\Users\wismi\HSE_gitlab>git clone https://gitlab.com/hse-public/dff_politics_skill.git

  (dff_politics_skill) C:\Users\wismi\HSE_gitlab>cd dff_politics_skill
```

2. Install dependencies 
```
  (dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>pip install -r requirements.txt
```  

3. Now we could have options to run in command line mode or web browser.
   To run on command line:  
```
  (dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_interactive.py 
```

Then type 'Hi' (English) or 'Hai' (Indonesian) and continue with the chats. 

4. To run on Web browser:
```
  (dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>uvicorn api:app
INFO:     Started server process [4184]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
``` 
Then open browser at `localhost:8000` and type 'Hi' or 'Hai' 

  ![image info](./images/chatbots_indonesia.png)


### - Sample Integration test output
- Putin 
```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_test.py
in_request='Tell me about Politicians' -> Welcome to politicians subtopics.
You could move to other subtopics with:
Tell me about countries
Tell me about terminology
Tell me about wars

Or move subtopic to specific politician (Putin, Xi Jinping):
Tell me about Putin

Or start from beginning:
Please restart
Start from beginning

in_request='Tell me about Putin' -> vladimir vladimirovich putin
in_request='who leads Russia?' -> vladimir vladimirovich putin
in_request='what is capital of Russia?' -> moscow
in_request='who is russian president?' -> vladimir vladimirovich putin
in_request='who is russian president before Putin?' -> boris yeltsin
in_request='who is Putin?' -> a russian politician and former intelligence officer who is serving as the current president of russia
in_request='when did Putin born' -> 7 october 1952
in_request='Is Putin a KGB agent' -> putin worked as a kgb foreign intelligence officer for 16 years , rising to the rank of lieutenant colonel
in_request='does Putin support the church' -> putin has collaborated closely with the russian orthodox church
in_request='does russian church support Putin' -> putin has collaborated closely with the russian orthodox church . patriarch kirill of moscow , head of the church , endorsed his election in 2012 stating putin ' s terms were like " a miracle of god . "
in_request='who lead russian orthodox church' -> patriarch kirill of moscow
in_request='what Kirill say about Putin' -> putin ' s terms were like " a miracle of god . "
in_request='did putin wins election' -> putin received 76 % of the vote in the 2018 election and was re - elected for a six - year term ending in 2024 .
in_request='Is Putin conservative?' -> putin has attacked globalism and neo - liberalism and is identified by scholars with russian conservatism
in_request='is russia prosperous under putin' -> russian economy grew for eight straight years
in_request='what did Putin do internationally' -> putin led russia to victory in the second chechen war and the russo - georgian war
translated to: Who is the leader of Russia?
in_request='siapa pemimpin Russia?' -> vladimir vladimirovich putin
translated to: What is the capital of Russia?
in_request='Apa ibukota Russia?' -> Moskwa
translated to: Who is the president of Russia?
in_request='siapa presiden Russia?' -> vladimir vladimirovich putin
translated to: Who was president of Russia before Putin?
in_request='siapa presiden Russia sebelum Putin' -> boris yeltsin
translated to: Who is Putin?
in_request='siapa itu Putin?' -> Seorang politisi Rusia dan mantan perwira intelijen yang menjabat sebagai presiden Rusia saat ini
translated to: When was Putin born?
in_request='kapan Putin lahir?' -> 7 Oktober 1952
translated to: Is Putin a KGB agent?
in_request='Apakah Putin agen KGB?' -> putin bekerja sebagai perwira intelijen asing kgb selama 16 tahun
translated to: Does the Russian church support Putin?
in_request='Apakah gereja Russia mendukung Putin?' -> Putin telah bekerja sama erat dengan gereja ortodoks Rusia. Patriark Kirill dari Moskow, kepala gereja, mendukung pemilihannya pada tahun 2012 yang menyatakan bahwa istilah Putin seperti "keajaiban Tuhan."
translated to: Does Putin support the church?
in_request='Apakah Putin mendukung gereja?' -> Putin telah bekerja sama erat dengan gereja ortodoks Rusia
translated to: Who is the leader of the Russian Othodoks church?
in_request='Siapa pemimpin gereja Othodoks Russia?' -> patriark kirill dari Moskow
translated to: What is Kirill's statement about Putin?
in_request='apa pernyataan Kirill tentang Putin' -> Istilah Putin seperti "keajaiban Tuhan."
translated to: Did Putin win the election?
in_request='Apakah Putin menang dalam pemilu?' -> Putin menerima 76% suara dalam pemilihan 2018 dan terpilih kembali untuk masa jabatan enam tahun yang berakhir pada 2024.
translated to: Is Putin conservative?
in_request='apakah Putin konservatif?' -> Putin telah menyerang globalisme dan neo-liberalisme dan diidentifikasi oleh para sarjana dengan konservatisme Rusia.
translated to: Has Russia improved under Putin?
in_request='Apakah Russia membaik dibawah Putin?' -> Ekonomi Rusia tumbuh selama delapan tahun berturut-turut
translated to: What Putin is doing in the international world
in_request='Apa yang Putin lakukan dalam dunia International' -> Putin memimpin Rusia menuju kemenangan dalam perang Chechnya kedua dan perang Rusia - Georgia
```

- Indonesia 
```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_test.py
in_request='Tell me about country' -> Welcome to countries subtopics.
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China

Or start from beginning:
Please restart
Start from beginning

in_request='Tell me about Indonesia' -> indonesia is a presidential , constitutional republic with an elected legislature
in_request='How Indonesia become independent' -> upon japanese surrender , sukarno and mohammad hatta declared indonesian independence on 17 august 1945
in_request='who is first president of indonesia' -> sukarno
in_request='who is Sukarno?' -> an indonesian statesman , politician , nationalist , and assimilationist
in_request='when was Indonesia independent?' -> 17 august 1945
in_request='When did Sukarno died?' -> 21 june 1970
in_request='When Indonesia independence is recognized?' -> 1949
in_request='Did Sukarno unify the country?' -> sukarno was the only asian leader of the modern era able to unify people of such differing ethnic , cultural and religious backgrounds without shedding a drop of blood
in_request='Who colonized Indonesia?' -> dutch
in_request='Who help Suharto to coup Sukarno?' -> cia and british intelligence services
in_request='How many Suharto killed in Indonesia?' -> 500 , 000 to over 1 , 000 , 000
in_request='who is current president of Indonesia?' -> joko widodo
translated to: How independence can be achieved
in_request='bagaimana kemerdekaan bisa tercapai' -> Setelah Jepang menyerah, Soekarno dan Mohammad Hatta mendeklarasikan kemerdekaan Indonesia pada tanggal 17 Agustus 1945.
translated to: Who was the first president of ri?
in_request='siapa presiden pertama RI?' -> Sukarno
translated to: Who was Sukarno?
in_request='siapa sukarno?' -> seorang negarawan Indonesia, politisi, nasionalis, dan asimilasi yang merupakan presiden pertama Indonesia
translated to: When did Indonesia declare independence?
in_request='kapan Indonesia menyatakan kemerdekaan?' -> 17 Agustus 1945
translated to: When did Sukarno die?
in_request='kapan sukarno meninggal?' -> 21 Juni 1970
translated to: When was Indonesia's independence recognized?
in_request='kapan kemerdekaan Indonesia diakui?' -> 1949
translated to: Did Sukarno unite Indonesia?
in_request='apakah Sukarno menyatukan Indonesia?' -> Sukarno adalah satu-satunya pemimpin Asia dari era modern yang mampu menyatukan orang-orang dari latar belakang etnis, budaya dan agama yang berbeda tanpa menumpahkan setetes darah.
translated to: Who colonized Indonesia?
in_request='Siapa menjajah Indonesia?' -> Belanda
translated to: Who helped Suharto overthrow Sukarno?
in_request='Siapa membantu Suharto menggulingkan Sukarno?' -> Cia dan Dinas Intelijen Inggris
translated to: How many were killed by Suharto?
in_request='Berapa banyak yang dibunuh Suharto?' -> 500, 000 hingga lebih dari 1.000 , 000
translated to: Who is the president of Indonesia now?
in_request='Siapa presiden Indonesia sekarang?' -> joko widodo
translated to: Who is Joko Widodo?
in_request='Siapa Joko Widodo?' -> Presiden indonesia ke-7 dan saat ini
```

- Libya 
```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_test.py
in_request='Tell me about country' -> Welcome to countries subtopics.
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China

Or start from beginning:
Please restart
Start from beginning

in_request='Tell me about Libya' -> libya is a member of the united nations ( since 1955 ) , the non - aligned movement , the arab league , oic and opec .
in_request='Who is current leader of Libya?' -> mohamed yunus al - menfi
in_request='Who is Mohamed Yunus al-menfi?' -> interim president of libya
in_request='When Mohamed Yunus al-menfi become president?' -> 5 february 2021
in_request='What Mohamed Yunus al-menfi did before becoming president?' -> libyan ambassador to greece
in_request='Was there civil war in Libya?' -> libya has encountered two civil wars
in_request='What is the first civil war in Libya?' -> the first libyan civil war
in_request='what is first libyan civil war?' -> rebel forces launched an offensive on the government - held coast of libya , backed by a wide - reaching nato bombing campaign , ultimately capturing the capital city of tripoli
in_request='when first libyan civil war occured?' -> august 2011
in_request='Who was president of Libya during first Libyan civil war?' -> muammar gaddafi
in_request='What is the result of first libyan civil war?' -> capturing the capital city of tripoli
in_request='What happend to muammar al-gaddafi during civil war?' -> captured and killed
in_request='How long Muammar Gaddafi ruled Libya?' -> 1969 - 2011
in_request='What are the titles of Muammar Gaddafi?' -> revolutionary chairman of the libyan arab republic from 1969 to 1977 and then as the brotherly leader of the great socialist people ' s libyan arab jamahiriya
in_request='Who backed the rebel during first Libya civil war?' -> nato bombing campaign
in_request='What NATO did during first Libyan civil war?' -> bombing campaign
in_request='What is second Libyan civil war?' -> a multi - sided civil war
in_request='When second Libya civil war happened?' -> 2014 to 2020
in_request='When the second Libyan civil war ended?' -> 23 october 2020
in_request='How the second Libyan civil war ended?' -> reached a permanent ceasefire agreement
in_request='Who ended the second Libyan civil war?' -> the 5 + 5 joint libyan military commission
translated to: Who is the leader of Libya now?
in_request='Siapa pemimpin Libya sekarang?' -> mohamed yunus al - menfi
translated to: Who is Mohamed Yunus al-menfi?
in_request='Siapa Mohamed Yunus al-menfi?' -> Presiden interim Libya
translated to: When did Jonah al-Menfi become president?
in_request='Kapan Yunus al-menfi menjadi presiden?' -> 5 Februari 2021
translated to: What was Mohamed Yunus al-Menfi's job before he became president?
in_request='Apa pekerjaan Mohamed Yunus al-menfi sebelum menjadi presiden?' -> Duta Besar Libya untuk Yunani
translated to: Is there a civil war in Libya?
in_request='Apakah di Libya ada perang sipil?' -> Libya telah mengalami dua perang saudara
translated to: Is there a civil war in Libya?
in_request='Apakah di Libya ada perang saudara?' -> Libya telah mengalami dua perang saudara
translated to: What happened to the first civil war in Libya?
in_request='Apa yang terjadi para perang saudara pertama di Libya?' -> Pasukan pemberontak melancarkan serangan terhadap pemerintah - yang dikuasai pantai Libya, yang didukung oleh kampanye pemboman NATO yang luas, yang pada akhirnya merebut ibu kota Tripoli.
translated to: When was the first civil war in Libya?
in_request='Kapan terjadi perang saudara pertama di Libya?' -> Agustus 2011
translated to: Who was the leader of Libya during the first civil war?
in_request='Siapa pemimpin Libya saat terjadi perang saudara pertama?' -> muammar gaddafi
translated to: What was the outcome of the first civil war in Libya?
in_request='Apa hasil dari perang saudara pertama di Libya?' -> Merebut ibu kota Tripoli
translated to: What was the fate of Muammar Gaddafi during the civil war?
in_request='Bagaimana nasib Muammar Gaddafi saat perang saudara?' -> Ditangkap dan dibunuh
translated to: How long has Muammar Gaddafi been in power in Libya?
in_request='Berapa lama Muammar Gaddafi berkuasa di Libya?' -> 1969 - 2011
translated to: What is muammar gaddafi?
in_request='Apa saja sebutan untuk Muammar Gaddafi?' -> Seorang revolusioner Libya, politisi dan ahli teori politik
translated to: What is muammar gaddafi?
in_request='Apa saja title Muammar Gaddafi?' -> Seorang revolusioner Libya, politisi dan ahli teori politik
translated to: Who supported the rebels in the first civil war in Libya?
in_request='Siapa mendukung pemberontak pada perang sipil pertama di Libya?' -> Kampanye pengeboman NATO
translated to: What did NATO do in the first civil war in Libya?
in_request='Apa yang NATO lakukan pada perang sipil pertama di Libya?' -> kampanye pengeboman
translated to: Who were the rebels' supporters in Libya's first civil war?
in_request='Siapa pendukung pemberontak pada perang sipil pertama di Libya?' -> Kampanye pengeboman NATO
translated to: What is the second civil war in Libya?
in_request='Apa itu perang saudara kedua di Libya?' -> perang saudara multi-sisi yang berlangsung dari 2014 hingga 2020
translated to: When is the second civil war in Libya?
in_request='Kapan perang saudara kedua di Libya?' -> 2014 hingga 2020
translated to: When did the second civil war in Libya end?
in_request='Kapan perang saudara kedua di Libya berakhir?' -> 23 Oktober 2020
translated to: When did the civil war in Libya end?
in_request='Kapan perang saudara di Libya berakhir?' -> 23 Oktober 2020
translated to: How did the second civil war in Libya end?
in_request='Bagaimana perang saudara kedua di Libya berakhir?' -> perjanjian gencatan senjata permanen
translated to: Who ended the second civil war in Libya?
in_request='Siapa yang mengakhiri perang saudara kedua di Libya?' -> Komisi militer gabungan Libya 5+5
```

- Cold War
```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_test.py
in_request='Tell me about wars' -> Welcome to wars subtopics.
You could move to other subtopics with:
Tell me about countries
Tell me about terminology

Or move subtopic to specific wars (world war 1, world war 2, cold war):
Tell me about cold war

Or start from beginning:
Please restart
Start from beginning

in_request='Tell me about cold war' -> the cold war was a period of geopolitical tension between the united states and the soviet union and their respective allies , the western bloc and the eastern bloc , which began following world war ii . the cold war period is generally considered to span the 1947 truman doctrine ( 12 march 1947 ) to the 1991 dissolution of the soviet union ( 26 december 1991 )
in_request='What is cold war' -> the cold war was a period of geopolitical tension between the united states and the soviet union and their respective allies , the western bloc and the eastern bloc , which began following world war ii . the cold war period is generally considered to span the 1947 truman doctrine ( 12 march 1947 ) to the 1991 dissolution of the soviet union ( 26 december 1991 )
in_request='How the cold war started' -> following world war ii
in_request='How the cold war ended' -> 1991 dissolution of the soviet union ( 26 december 1991 )
in_request='Who won the cold war' -> the united states
in_request='What was Western bloc?' -> liberal democratic but tied to a network of the authoritarian states
in_request='What was Eastern bloc?' -> soviet union and its communist party
in_request='What did United States do during cold war' -> united states and its allies created the nato military alliance in 1949
in_request='What did Soviet Union do during cold war' -> soviet union formed the warsaw pact in 1955 in response to nato
in_request='What is NATO' -> united states and its allies created the nato military alliance
in_request='What is warsaw pact' -> the soviet union formed the warsaw pact
in_request='What crisises during cold war' -> 1948 – 49 berlin blockade , the 1927 – 1949 chinese civil war , the 1950 – 1953 korean war , the 1956 hungarian revolution , the 1956 suez crisis , the berlin crisis of 1961 and the 1962 cuban missile crisis
in_request='Why they use the term cold war' -> there was no large - scale fighting directly between the two superpowers
in_request='Who did United States support during cold war' -> united states government supported right - wing governments and uprisings across the world
in_request='Who did Soviet Union support' -> communist parties and revolutions around the world
in_request='What was Cuban crisis' -> 1962 cuban missile crisis
in_request='What happened after cuban crisis' -> sino - soviet split between china and the soviet union complicate relations within the communist sphere
in_request="Who lead Soviet Union during it's collapse" -> mikhail gorbachev
in_request='What did Michail Gorbachev do' -> introduced the liberalizing reforms of glasnost ( " openness " , c . 1985 ) and perestroika ( " reorganization " , 1987 ) and ended soviet involvement in afghanistan in 1989
in_request='What liberalizing reforms by Michail Gorbachev' -> glasnost ( " openness " , c . 1985 ) and perestroika ( " reorganization " , 1987 )
in_request='How did Soviet Union collapse' -> 1991 dissolution of the soviet union ( 26 december 1991 )
translated to: What is a Cold War?
in_request='Apa itu perang dingin' -> Periode ketegangan geopolitik antara Amerika Serikat dan Uni Soviet dan sekutu masing-masing, blok barat dan blok timur, yang dimulai setelah Perang Dunia II. Periode perang dingin umumnya dianggap mencakup doktrin truman 1947 (12 Maret 1947) hingga pembubaran Uni Soviet 1991 (26 Desember 1991). Istilah perang dingin digunakan karena tidak ada pertempuran skala besar langsung antara dua negara adidaya.
translated to: When did the Cold War happen?
in_request='Kapan perang dingin terjadi' -> Dimulai setelah Perang Dunia II
translated to: When did the Cold War end?
in_request='Kapan perang dingin berakhir' -> 1991 pembubaran Uni Soviet (26 Desember 1991)
translated to: Who was involved in the Cold War?
in_request='Siapa yang terlibat dalam perang dingin' -> Amerika Serikat dan Uni Soviet
translated to: Who won the Cold War?
in_request='Siapa memenangkan perang dingin' -> Amerika Serikat
translated to: What is the Western bloc?
in_request='Apa itu bloc Barat' -> Blok barat dipimpin oleh Amerika Serikat.
translated to: What is the Eastern bloc?
in_request='Apa itu bloc Timur' -> Blok timur dipimpin oleh Uni Soviet dan partai komunisnya.
translated to: What America did during the Cold War
in_request='Apa yang Amerika lakukan masa perang dingin' -> Amerika Serikat ditinggalkan sebagai satu-satunya negara adidaya di dunia sebagai pemenang perang dingin.
translated to: What did America do during the Cold War?
in_request='Apa yang Amerika lakukan masa perang dingin?' -> Membentuk Aliansi Militer NATO
translated to: What did the Soviet Union do during the Cold War?
in_request='Apa yang Uni Soviet lakukan masa perang dingin?' -> Membentuk pakta Warsawa
translated to: What is NATO?
in_request='Apa itu NATO' -> Amerika Serikat dan sekutunya membentuk aliansi militer NATO.
translated to: What is the Warsaw Pact?
in_request='Apa itu pakta Warsawa' -> Uni Soviet membentuk pakta Warsawa pada tahun 1955 sebagai tanggapan terhadap NATO.
translated to: What are the major crises of the Cold War?
in_request='Apa saja krisis besar dalam perang dingin' -> Tahun 1948 - Blokade Berlin, perang saudara Cina 1927- 1949, perang Korea 1950 -1953, revolusi Hongaria 1956, krisis suez 1956, krisis Berlin 1961 dan krisis rudal Kuba 1962.
translated to: Why the term Cold War is used
in_request='Kenapa istilah perang dingin dipakai' -> tidak ada pertempuran skala besar langsung antara dua negara adidaya
translated to: What happened after the Cuban crisis?
in_request='Apa yang terjadi setelah krisis Kuba' -> Sino - perpecahan Soviet antara Cina dan Uni Soviet mempersulit hubungan dalam lingkup komunis
translated to: What happened in the second phase of the Cold War?
in_request='Apa yang terjadi pada fase kedua perang dingin' -> perpecahan sino - soviet antara Cina dan Uni Soviet mempersulit hubungan dalam lingkup komunis
translated to: What is the Cuban crisis?
in_request='Apa itu krisis Kuba' -> Krisis rudal Kuba 1962
translated to: Who was the leader of the Soviet Union when it broke up?
in_request='Siapa pemimpin Uni Soviet sewaktu bubar' -> mikhail gorbachev
translated to: What Mikhail Gorbachev did
in_request='Apa yang Mikhail Gorbachev lakukan' -> memperkenalkan reformasi liberalisasi glasnost ("keterbukaan", c. 1985) dan perestroika ("reorganisasi", 1987)
translated to: How did the Soviet Union break up?
in_request='Bagaimana Uni Soviet bisa bubar?' -> Partai komunis uni soviet dilarang setelah upaya kudeta yang gagal.
```

- Monarchy
```
(dff_politics_skill) C:\Users\wismi\HSE_gitlab\dff_politics_skill>python run_test.py
in_request='Tell me about terminologies' -> Welcome to terminologies subtopics.
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning:
Please restart
Start from beginning

in_request='tell me about monarchy' -> monarchy is a form of government in which a person , the monarch , is head of state for life or until abdication
in_request='what is monarchy' -> a form of government in which a person , the monarch , is head of state for life or until abdication
in_request='how is monarchy succession' -> the succession of monarchs is in most cases hereditary
in_request='what is opposite of monarchy' -> absolute monarchy ) , and can expand across the domains of the executive , legislative , and judicial . monarchs can carry various titles such as emperor , empress , king , queen , raja , khan , tsar , sultan , shah , or pharaoh . the succession of monarchs is in most cases hereditary , often building dynastic periods . however , elective and self - proclaimed monarchies are possible . aristocrats , though not inherent to monarchies , often serve as the pool of persons to draw the monarch from and fill the constituting institutions ( e . g . diet and court ) , giving many monarchies oligarchic elements . monarchies were the most common form of government until the 20th century . today forty - three sovereign nations in the world have a monarch , including fifteen commonwealth realms that have elizabeth ii as their shared head of state . other than that there are a range of sub - national monarchical entities
in_request='how many tipe of monarchy' -> forty - three
in_request='how many country is monarch' -> forty - three
in_request='when is monarch popular' -> Sorry, I don't know about that.
in_request='when was monarchy most common form of government' -> until the 20th century
translated to: Tell me about the monarchy.
in_request='ceritakan tentan monarki' -> Monarki adalah bentuk pemerintahan di mana seseorang, raja, adalah kepala negara seumur hidup atau sampai turun tahta.
translated to: How many types of monarchies
in_request='ada berapa jenis monarki' -> monarki konstitusional
translated to: What kind of monarchy
in_request='apa jenis monarki' -> Monarki adalah bentuk pemerintahan di mana seseorang, raja, adalah kepala negara seumur hidup atau sampai turun tahta. Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut), dan dapat memperluas seluruh domain eksekutif, legislatif, dan yudikatif. Raja dapat membawa berbagai gelar seperti kaisar, permaisuri, raja, ratu, raja, khan, tsar, sultan, shah, atau firaun. Suksesi raja dalam banyak kasus turun-temurun, sering membangun periode dinasti. namun, monarki elektif dan memproklamirkan diri adalah mungkin. aristokrat, meskipun tidak melekat pada monarki, sering berfungsi sebagai kumpulan orang untuk menarik raja dari dan mengisi lembaga-lembaga konstituitusi (misalnya diet dan pengadilan), memberikan banyak elemen oligarki monarki. Monarki adalah bentuk pemerintahan yang paling umum sampai abad ke-20. Hari ini empat puluh - tiga negara berdaulat di dunia memiliki raja, termasuk lima belas alam persemakmuran yang memiliki elizabeth II sebagai kepala negara bersama mereka. selain itu ada berbagai entitas monarki nasional sub-nasional. Monarki modern cenderung menjadi monarki konstitusional
translated to: What is the opposite of monarchy?
in_request='apa kebalikan dari monarki' -> monarki absolut
translated to: How to change the monarchy
in_request='bagaimana pergantian monarki' -> Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut).
translated to: When is monarchy very common?
in_request='kapan monarki sangat umum' -> Monarki adalah bentuk pemerintahan yang paling umum sampai abad ke-20.
translated to: How to change power in the monarchy
in_request='bagaimana pergantiaan kekuasaan dalam monarki' -> Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut), dan dapat memperluas seluruh domain eksekutif, legislatif, dan yudikatif.
```


## Quickstart

1. Set env variable `azure_translate_key` (Win) or `AZURE_TRANSLATE_KEY` (Linux/MacOS) with valid api key. 

2. Install dependencies
```bash
pip install -r requirements.txt
```
3. Run interactive mode
```bash
python run_interactive.py
```
4. Or Run tests
```bash
python run_test.py
```
## Resources
* Execution time: about 4 seconds when using huggingface model. 
* Starting time: about 30 seconds (docker container)
* RAM: 3000 MB


## Known issues 

1. Sometimes this issue show up on command line, just retry again and usually it works the second time. 
```
    raise HTTPError(http_error_msg, response=self)
requests.exceptions.HTTPError: 504 Server Error: Gateway Time-out for url: https://huggingface.co/api/models/bert-large-uncased-whole-word-masking-finetuned-squad
INFO:     Stopping reloader process [5864]
```

2. Sometimes the model don't have sufficient data to answer properly. E.g. 
```
Question: who won the cold war
Answer: the united states
exec time = 3.000s
type your request: Who lost the cold war

Question: Who lost the cold war
Answer: Sorry, I don't know about that.
exec time = 3.202s
type your request:
```        