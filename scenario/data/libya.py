libya_wiki_text = """
Libya is a member of the United Nations (since 1955), the Non-Aligned Movement, the Arab League, OIC and OPEC. 
The country's official religion is Islam, with 96.6% of the Libyan population being Sunni Muslims.

Muammar Muhammad Abu Minyar al-Gaddafi (c. 1942 – 20 October 2011) was a Libyan revolutionary, 
politician and political theorist. 
Muammar al-Gaddafi governed Libya as the de facto ruler (1969 - 2011) and had many titles such as Revolutionary Chairman of the Libyan Arab Republic from 1969 to 1977 and then as the Brotherly Leader of the Great Socialist People's Libyan Arab Jamahiriya from 1977 to 2011. 
Muammar al-Gaddafi was initially ideologically committed to Arab nationalism and Arab socialism but later ruled according to his own Third International Theory.

Muammar al-Gaddafi was born near Sirte, Italian Libya, to a poor Bedouin family, Gaddafi became an Arab nationalist while at school in Sabha, 
later enrolling in the Royal Military Academy, Benghazi. 
Within the military, Gaddafi founded a revolutionary group which deposed the Western-backed Senussi monarchy of Idris in a 1969 coup. 
Having taken power, Gaddafi converted Libya into a republic governed by his Revolutionary Command Council. 

Libya has encountered two civil wars. 

During the First Libyan Civil War, in August 2011, rebel forces launched an offensive on the government-held coast of Libya, backed by a wide-reaching NATO bombing campaign, ultimately capturing the capital city of Tripoli. 
On 20 October 2011, Muammar Gaddafi  was captured and killed in Sirte.

The Second Libyan Civil War was a multi-sided civil war that lasted from 2014 to 2020 in the North African country of Libya fought between different armed groups, mainly the House of Representatives and the Government of National Accord.
On 23 October 2020, the 5+5 Joint Libyan Military Commission representing the LNA and the GNA reached a permanent ceasefire agreement in all areas of Libya to end the second Libyan civil war. 

Mohamed Yunus al-Menfi (born March 3, 1976) is a Libyan diplomat and politician from Tobruk and current interim president of Libya. On 5 February 2021, al-Menfi was chosen as the president of the Libyan Presidential Council at the Libyan Political Dialogue Forum. 
Previously, he had served as the Libyan Ambassador to Greece.
"""

libya_testing_dialog = [
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Libya', 'libya is a member of the united nations ( since 1955 ) , the non - aligned movement , the arab league , oic and opec .'),
    ('Who is current leader of Libya?', 'mohamed yunus al - menfi'),
    ('Who is Mohamed Yunus al-menfi?', 'interim president of libya'),
    ('When Mohamed Yunus al-menfi become president?', '5 february 2021'),
    ('What Mohamed Yunus al-menfi did before becoming president?', 'libyan ambassador to greece'),
    ('Was there civil war in Libya?', 'libya has encountered two civil wars'),
    ('What is the first civil war in Libya?', 'the first libyan civil war'),
    ('what is first libyan civil war?', 'rebel forces launched an offensive on the government - held coast of libya , backed by a wide - reaching nato bombing campaign , ultimately capturing the capital city of tripoli'),
    ('when first libyan civil war occured?', 'august 2011'),
    ('Who was president of Libya during first Libyan civil war?', 'muammar gaddafi'),
    ('What is the result of first libyan civil war?', 'capturing the capital city of tripoli'),
    ('What happend to muammar al-gaddafi during civil war?', 'captured and killed'),
    ('How long Muammar Gaddafi ruled Libya?', '1969 - 2011'),
    ('What are the titles of Muammar Gaddafi?', "revolutionary chairman of the libyan arab republic from 1969 to 1977 and then as the brotherly leader of the great socialist people ' s libyan arab jamahiriya"),
    ('Who backed the rebel during first Libya civil war?', 'nato bombing campaign'),
    ('What NATO did during first Libyan civil war?', 'bombing campaign'),
    ('What is second Libyan civil war?', 'a multi - sided civil war'),
    ('When second Libya civil war happened?', '2014 to 2020'),
    ('When the second Libyan civil war ended?', '23 october 2020'),
    ('How the second Libyan civil war ended?', 'reached a permanent ceasefire agreement'),
    ('Who ended the second Libyan civil war?', 'the 5 + 5 joint libyan military commission'),
    # dalam Bahasa Indonesia
    ('Siapa pemimpin Libya sekarang?', 'mohamed yunus al - menfi'),
    ('Siapa Mohamed Yunus al-menfi?', 'Presiden interim Libya'),
    ('Kapan Yunus al-menfi menjadi presiden?', '5 Februari 2021'),
    ('Apa pekerjaan Mohamed Yunus al-menfi sebelum menjadi presiden?', 'Duta Besar Libya untuk Yunani'),
    ('Apakah di Libya ada perang sipil?', 'Libya telah mengalami dua perang saudara'),
    ('Apakah di Libya ada perang saudara?', 'Libya telah mengalami dua perang saudara'),
    ('Apa yang terjadi para perang saudara pertama di Libya?', 'Pasukan pemberontak melancarkan serangan terhadap pemerintah - yang dikuasai pantai Libya, yang didukung oleh kampanye pemboman NATO yang luas, yang pada akhirnya merebut ibu kota Tripoli.'),
    ('Kapan terjadi perang saudara pertama di Libya?', 'Agustus 2011'),
    ('Siapa pemimpin Libya saat terjadi perang saudara pertama?', 'muammar gaddafi'),
    ('Apa hasil dari perang saudara pertama di Libya?', 'Merebut ibu kota Tripoli'),
    ('Bagaimana nasib Muammar Gaddafi saat perang saudara?', 'Ditangkap dan dibunuh'),
    ('Berapa lama Muammar Gaddafi berkuasa di Libya?', '1969 - 2011'),
    ('Apa saja sebutan untuk Muammar Gaddafi?', 'Seorang revolusioner Libya, politisi dan ahli teori politik'),
    ('Apa saja title Muammar Gaddafi?', 'Seorang revolusioner Libya, politisi dan ahli teori politik'),
    ('Siapa mendukung pemberontak pada perang sipil pertama di Libya?', 'Kampanye pengeboman NATO'),
    ('Apa yang NATO lakukan pada perang sipil pertama di Libya?', 'kampanye pengeboman'),
    ('Siapa pendukung pemberontak pada perang sipil pertama di Libya?', 'Kampanye pengeboman NATO'),
    ('Apa itu perang saudara kedua di Libya?', 'perang saudara multi-sisi yang berlangsung dari 2014 hingga 2020'),    
    ('Kapan perang saudara kedua di Libya?', '2014 hingga 2020'),
    ('Kapan perang saudara kedua di Libya berakhir?', '23 Oktober 2020'),
    ('Kapan perang saudara di Libya berakhir?', '23 Oktober 2020'),
    ('Bagaimana perang saudara kedua di Libya berakhir?', 'perjanjian gencatan senjata permanen'),
    ('Siapa yang mengakhiri perang saudara kedua di Libya?', 'Komisi militer gabungan Libya 5+5'),
]