usa_wiki_text = """
The United States is a federal republic of 50 states, a federal district, five territories and several uninhabited island possessions.

In the American federalist system, citizens are usually subject to three levels of government: federal, state, and local. The local government's duties are commonly split between county and municipal governments. In almost all cases, executive and legislative officials are elected by a plurality vote of citizens by district.

The government is regulated by a system of checks and balances defined by the U.S. Constitution, which serves as the country's supreme legal document. 
The Constitution establishes the structure and responsibilities of the federal government and its relationship with the individual states. 
The Constitution has been amended 27 times; the first ten amendments, which make up the Bill of Rights, 
and the Fourteenth Amendment form the central basis of Americans' individual rights.

The federal government comprises three branches: Legislative, Executive and Judicial.

Legislative: The bicameral Congress, made up of the Senate and the House of Representatives, makes federal law, declares war, approves treaties, etc.
Executive: The president is the commander-in-chief of the military.
Judicial: The Supreme Court and lower federal courts, interpret laws and overturn those they find unconstitutional.

The House of Representatives has 435 voting members, each representing a congressional district for a two-year term. 
House seats are apportioned among the states by population. 
Each state then draws single-member districts to conform with the census apportionment. 
The District of Columbia and the five major U.S. territories each have one member of Congress—these members are not allowed to vote.

The Senate has 100 members with each state having two senators, elected at-large to six-year terms; one-third of Senate seats are up for election every two years. 
The District of Columbia and the five major U.S. territories do not have senators. 
The president serves a four-year term and may be elected to the office no more than twice. 
The president is not elected by direct vote, but by an indirect electoral college system in which the determining votes are apportioned to the states and the District of Columbia. 
The Supreme Court, led by the chief justice of the United States, has nine members, who serve for life.
"""

usa_testing_dialog = [
    ('Tell me about nations', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about america', 'the united states is a federal republic of 50 states , a federal district , five territories and several uninhabited island possessions .'),
    ('What citizen in USA subjected to', 'citizens are usually subject to three levels of government : federal , state , and local'),
    ('What regulate the government', 'the government is regulated by a system of checks and balances defined by the u . s . constitution'),
    ('What is constitution', 'u . s . constitution'),
    ('What is the use of constitution', 'the constitution establishes the structure and responsibilities of the federal government and its relationship with the individual states'),
    ('Can the constitution be ammended', 'the constitution has been amended 27 times'),
    ('Is there any special ammendment', 'the district of columbia and the five major u . s . territories each have one member of congress — these members are not allowed to vote'),
    ('What is federal government', 'the united states is a federal republic of 50 states , a federal district , five territories and several uninhabited island possessions'),
    ('What the federal government consist of', 'the federal government comprises three branches : legislative , executive and judicial'),
    ('What is legislative', 'the bicameral congress , made up of the senate and the house of representatives'),
    ('What is executive', 'the president is the commander - in - chief of the military'),
    ('What is judicial', 'the supreme court and lower federal courts , interpret laws and overturn those they find unconstitutional'),
    ('What is house of representative', 'the house of representatives has 435 voting members , each representing a congressional district for a two - year term'),
    ('What is senate', 'the senate has 100 members'),
    ('What is president', 'the president serves a four - year term'),
    ('Who is judicial', 'the supreme court and lower federal courts , interpret laws and overturn those they find unconstitutional'),
    ('who is supreme court', 'chief justice of the united states'),
    ('what supreme court do', 'the supreme court , led by the chief justice of the united states'),
    ('What is the function of supreme court ', 'interpret laws and overturn those they find unconstitutional'),
    ('what is the function of legislative', 'makes federal law , declares war , approves treaties , etc .'),
    ('What is the function of president', "Sorry, I don't know about that."),
    ('What president do', 'the president serves a four - year term'),
    ('Who is the president', 'the president serves a four - year term'),
    ('What is president', 'the president serves a four - year term'),
    ('Who is executive', 'the president is the commander - in - chief of the military'),
]
