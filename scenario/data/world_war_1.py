world_war_1_wiki_text = """
World War I was a international conflict that began on 28 July 1914 and ended on 11 November 1918. 
It involved much of Europe, as well as Russia, the United States and Turkey, and was also fought in the Middle East, Africa and parts of Asia. 
One of the deadliest conflicts in history, an estimated 9 million were killed in combat, while over 5 million civilians died from occupation, bombardment, hunger or disease.
The genocides perpetrated by the Ottomans and the 1918 Spanish flu pandemic spread by the movement of combatants during the war caused many millions of additional deaths worldwide.

In November 1914, the Ottoman Empire, Germany and Austria formed the Central Powers, 
while in April 1915, Italy joined Britain, France, Russia and Serbia as the Allied Powers. 
The Central Powers fights the Allied Powers during the world war I. 

The Central power began to collapse; Bulgaria signed an Armistice on 29 September, followed by the Ottomans on 31 October, then Austria-Hungary on 3 November. 
Isolated, facing revolution at home and an army on the verge of mutiny, Kaiser Wilhelm abdicated on 9 November and the new German government signed the Armistice of 11 November 1918, bringing the fighting to a close. 
The 1919 Paris Peace Conference imposed various settlements on the defeated powers, the best known being the Treaty of Versailles. 
World war I ended with the Allied Powers as winner and the dissolution of the Russian, German, Ottoman and Austro-Hungarian empires led to numerous uprisings and the creation of independent states, including Poland, Czechoslovakia and Yugoslavia.
"""

world_war_1_testing_dialog = [
    ('Tell me about wars', '''Welcome to wars subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology

Or move subtopic to specific wars (world war 1, world war 2, cold war):
Tell me about cold war

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about world war 1', 'world war i was a international conflict that began on 28 july 1914 and ended on 11 november 1918 . it involved much of europe , as well as russia , the united states and turkey , and was also fought in the middle east , africa and parts of asia . one of the deadliest conflicts in history'),
    ('Who were involved in WW 1', 'world war i was a international conflict that began on 28 july 1914 and ended on 11 november 1918 . it involved much of europe , as well as russia , the united states and turkey'),
    ('Who was involved in world war 1', 'it involved much of europe , as well as russia , the united states and turkey'),
    ('Who were involved in world war 1', 'it involved much of europe , as well as russia , the united states and turkey'),
    ('Who fight who in the WW 1', 'world war i was a international conflict that began on 28 july 1914 and ended on 11 november 1918 . it involved much of europe , as well as russia , the united states and turkey'),
    ('Who fights who in the WW 1', 'the central powers fights the allied powers'),
    ('Who were the central powers', 'the ottoman empire , germany and austria'),
    ('Who were the allied powers', 'italy joined britain , france , russia and serbia'),
    ('When the world war 1 ended', '11 november 1918'),
    ('How the world war 1 ended', 'world war i ended with the allied powers as winner and the dissolution of the russian , german , ottoman and austro - hungarian empires'),
    ('Who won the world war 1', 'the allied powers'),
    ('Who were involved in WW 1', 'world war i was a international conflict that began on 28 july 1914 and ended on 11 november 1918 . it involved much of europe , as well as russia , the united states and turkey'),
    ('Who was involved in world war 1', 'it involved much of europe , as well as russia , the united states and turkey'),
    ('Who were involved in world war 1', 'it involved much of europe , as well as russia , the united states and turkey'),
    ('Who fights who in the WW 1', 'the central powers fights the allied powers'),
    ('Who were the central powers', 'the ottoman empire , germany and austria'),
    ('Who were the allied powers', 'italy joined britain , france , russia and serbia'),
    ('When the world war 1 ended', '11 november 1918'),
    ('How the world war 1 ended', 'world war i ended with the allied powers as winner and the dissolution of the russian , german , ottoman and austro - hungarian empires'),
    ('Who won the world war 1', 'the allied powers'),
    ('Ceritakan tentang perang dunia 1', 'Perang dunia I adalah konflik internasional yang dimulai pada tanggal 28 Juli 1914 dan berakhir pada tanggal 11 November 1918. Ini melibatkan sebagian besar Eropa, serta Rusia, Amerika Serikat dan Turki, dan juga bertempur di Timur Tengah, Afrika dan sebagian Asia. Salah satu konflik paling mematikan dalam sejarah'),
    ('Siapa yang terlibat dalam perang dunia 1', 'ini melibatkan sebagian besar Eropa, serta Rusia, Amerika Serikat dan Turki.'),
    ('Siapa melawan siapa dalam perang dunia 1', 'Perang dunia I adalah konflik internasional yang dimulai pada tanggal 28 Juli 1914 dan berakhir pada tanggal 11 November 1918. ini melibatkan sebagian besar Eropa, serta Rusia, Amerika Serikat dan Turki.'),
    ('Siapa perang melawan siapa dalam perang dunia 1', 'Perang dunia I adalah konflik internasional yang dimulai pada tanggal 28 Juli 1914 dan berakhir pada tanggal 11 November 1918. ini melibatkan sebagian besar Eropa, serta Rusia, Amerika Serikat dan Turki.'),
    ('Kekuatan apa melawan kekuatan apa dalam perang dunia 1', 'Kekuatan sekutu'),
    ('Siapa lawan kekuatan sekutu', 'Kekuatan sentral'),
    ('Siapa membentuk kekuatan sekutu', 'Italia bergabung dengan Inggris, Prancis, Rusia dan Serbia sebagai kekuatan sekutu.'),
    ('Siapa membentuk kekuatan sentral', 'Kekaisaran Ottoman, Jerman dan Austria'),
    ('Kapan perang dunia 1 berakhir', '11 November 1918'),
    ('Bagaimana perang dunia 1 berakhir', 'Perang dunia I berakhir dengan kekuatan sekutu sebagai pemenang'),
    ('Siapa menang dalam perang dunia 1', 'Kekuatan sekutu'),
    ('Siapa yang terlibat dalam perang dunia 1', 'ini melibatkan sebagian besar Eropa, serta Rusia, Amerika Serikat dan Turki.'),
]