monarchy_wiki_text = """
A monarchy is a form of government in which a person, the monarch, is head of state for life or until abdication. 
The political legitimacy and authority of the monarch may vary from restricted and largely symbolic (constitutional monarchy), to fully autocratic (absolute monarchy), and can expand across the domains of the executive, legislative, and judicial. 
Monarchs can carry various titles such as emperor, empress, king, queen, raja, khan, tsar, sultan, shah, or pharaoh.

The succession of monarchs is in most cases hereditary, often building dynastic periods. 
However, elective and self-proclaimed monarchies are possible. 
Aristocrats, though not inherent to monarchies, often serve as the pool of persons to draw the monarch from and fill the constituting institutions (e.g. diet and court), giving many monarchies oligarchic elements.

Monarchies were the most common form of government until the 20th century. 
Today forty-three sovereign nations in the world have a monarch, including fifteen Commonwealth realms that have Elizabeth II as their shared head of state. 
Other than that there are a range of sub-national monarchical entities. 
Modern monarchies tend to be constitutional monarchies, retaining under a constitution unique legal and ceremonial roles for the monarch, exercising limited or no political power, similar to heads of state in a parliamentary republic.
"""

monarchy_testing_dialog = [
    ('Tell me about terminologies', '''Welcome to terminologies subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about monarchy', 'monarchy is a form of government in which a person , the monarch , is head of state for life or until abdication'),
    ('what is monarchy', 'a form of government in which a person , the monarch , is head of state for life or until abdication'),
    ('how is monarchy succession', 'the succession of monarchs is in most cases hereditary'),
    ('what is opposite of monarchy', 'absolute monarchy ) , and can expand across the domains of the executive , legislative , and judicial . monarchs can carry various titles such as emperor , empress , king , queen , raja , khan , tsar , sultan , shah , or pharaoh . the succession of monarchs is in most cases hereditary , often building dynastic periods . however , elective and self - proclaimed monarchies are possible . aristocrats , though not inherent to monarchies , often serve as the pool of persons to draw the monarch from and fill the constituting institutions ( e . g . diet and court ) , giving many monarchies oligarchic elements . monarchies were the most common form of government until the 20th century . today forty - three sovereign nations in the world have a monarch , including fifteen commonwealth realms that have elizabeth ii as their shared head of state . other than that there are a range of sub - national monarchical entities'),
    ('how many tipe of monarchy', 'forty - three'),
    ('how many country is monarch', 'forty - three'),
    ('when is monarch popular', "Sorry, I don't know about that."),
    ('when was monarchy most common form of government', 'until the 20th century'),
    ('ceritakan tentan monarki', 'Monarki adalah bentuk pemerintahan di mana seseorang, raja, adalah kepala negara seumur hidup atau sampai turun tahta.'),
    ('ada berapa jenis monarki', 'monarki konstitusional'),
    ('apa jenis monarki', 'Monarki adalah bentuk pemerintahan di mana seseorang, raja, adalah kepala negara seumur hidup atau sampai turun tahta. Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut), dan dapat memperluas seluruh domain eksekutif, legislatif, dan yudikatif. Raja dapat membawa berbagai gelar seperti kaisar, permaisuri, raja, ratu, raja, khan, tsar, sultan, shah, atau firaun. Suksesi raja dalam banyak kasus turun-temurun, sering membangun periode dinasti. namun, monarki elektif dan memproklamirkan diri adalah mungkin. aristokrat, meskipun tidak melekat pada monarki, sering berfungsi sebagai kumpulan orang untuk menarik raja dari dan mengisi lembaga-lembaga konstituitusi (misalnya diet dan pengadilan), memberikan banyak elemen oligarki monarki. Monarki adalah bentuk pemerintahan yang paling umum sampai abad ke-20. Hari ini empat puluh - tiga negara berdaulat di dunia memiliki raja, termasuk lima belas alam persemakmuran yang memiliki elizabeth II sebagai kepala negara bersama mereka. selain itu ada berbagai entitas monarki nasional sub-nasional. Monarki modern cenderung menjadi monarki konstitusional'),
    ('apa kebalikan dari monarki', 'monarki absolut'),
    ('bagaimana pergantian monarki', 'Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut).'),
    ('kapan monarki sangat umum', 'Monarki adalah bentuk pemerintahan yang paling umum sampai abad ke-20.'),
    ('bagaimana pergantiaan kekuasaan dalam monarki', 'Legitimasi politik dan otoritas raja dapat bervariasi dari terbatas dan sebagian besar simbolis (monarki konstitusional), untuk sepenuhnya otokratis (monarki absolut), dan dapat memperluas seluruh domain eksekutif, legislatif, dan yudikatif.'),
]