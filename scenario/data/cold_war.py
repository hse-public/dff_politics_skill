cold_war_wiki_text = """
The Cold War was a period of geopolitical tension between the United States and the Soviet Union and their respective allies, 
the Western Bloc and the Eastern Bloc, which began following World War II. 
The Cold war period is generally considered to span the 1947 Truman Doctrine (12 March 1947) to the 1991 Dissolution of the Soviet Union (26 December 1991).
The term cold war is used because there was no large-scale fighting directly between the two superpowers, but they each supported major regional conflicts known as proxy wars. 

The Western Bloc was led by the United States as well as the other First World nations of the Western Bloc that were generally liberal democratic but tied to a network of the authoritarian states, 
most of which were their former colonies. 
The Eastern Bloc was led by the Soviet Union and its Communist Party. 
The United States government supported right-wing governments and uprisings across the world, while the Soviet government funded communist parties and revolutions around the world. 

During first phase of Cold war, United States and its allies created the NATO military alliance in 1949 in the apprehension of a Soviet attack and Soviet influence containment. 
The Soviet Union formed the Warsaw Pact in 1955 in response to NATO. Major crises of the first phase of the Cold War included the 1948–49 Berlin Blockade, 
the 1927–1949 Chinese Civil War, the 1950–1953 Korean War, the 1956 Hungarian Revolution, the 1956 Suez Crisis, the Berlin Crisis of 1961 and the 1962 Cuban Missile Crisis. 

Following the Cuban Missile Crisis, second phase of Cold War began that saw the Sino-Soviet split between China and the Soviet Union complicate relations within the Communist sphere, 
while France, a Western Bloc state, began to demand greater autonomy of action. 

In the mid-1980s, the new Soviet leader Mikhail Gorbachev introduced the liberalizing reforms of glasnost ("openness", c. 1985) and perestroika ("reorganization", 1987) and ended Soviet involvement in Afghanistan in 1989. 
Pressures for national sovereignty grew stronger in Eastern Europe, and Gorbachev refused to militarily support their governments any longer.

The Communist Party of the Soviet Union was banned following an abortive coup attempt in August 1991, this led to the formal dissolution of the USSR in December 1991.
The United States was left as the world's only superpower as the winner of the Cold War.
"""


cold_war_testing_dialog = [
    # in English
    ('Tell me about wars', '''Welcome to wars subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology

Or move subtopic to specific wars (world war 1, world war 2, cold war):
Tell me about cold war

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about cold war', 'the cold war was a period of geopolitical tension between the united states and the soviet union and their respective allies , the western bloc and the eastern bloc , which began following world war ii . the cold war period is generally considered to span the 1947 truman doctrine ( 12 march 1947 ) to the 1991 dissolution of the soviet union ( 26 december 1991 )'),
    ('What is cold war', 'the cold war was a period of geopolitical tension between the united states and the soviet union and their respective allies , the western bloc and the eastern bloc , which began following world war ii . the cold war period is generally considered to span the 1947 truman doctrine ( 12 march 1947 ) to the 1991 dissolution of the soviet union ( 26 december 1991 )'),
    ('How the cold war started', 'following world war ii'),
    ('How the cold war ended', '1991 dissolution of the soviet union ( 26 december 1991 )'),
    ('Who won the cold war', 'the united states'),
    ('What was Western bloc?', 'liberal democratic but tied to a network of the authoritarian states'),
    ('What was Eastern bloc?', 'soviet union and its communist party'),
    ('What did United States do during cold war', 'united states and its allies created the nato military alliance in 1949'),
    ('What did Soviet Union do during cold war', 'soviet union formed the warsaw pact in 1955 in response to nato'),
    ('What is NATO', 'united states and its allies created the nato military alliance'),
    ('What is warsaw pact', 'the soviet union formed the warsaw pact'),
    ('What crisises during cold war', '1948 – 49 berlin blockade , the 1927 – 1949 chinese civil war , the 1950 – 1953 korean war , the 1956 hungarian revolution , the 1956 suez crisis , the berlin crisis of 1961 and the 1962 cuban missile crisis'),
    ('Why they use the term cold war', 'there was no large - scale fighting directly between the two superpowers'),
    ('Who did United States support during cold war', 'united states government supported right - wing governments and uprisings across the world'),
    ('Who did Soviet Union support', 'communist parties and revolutions around the world'),
    ('What was Cuban crisis', '1962 cuban missile crisis'),
    ('What happened after cuban crisis', 'sino - soviet split between china and the soviet union complicate relations within the communist sphere'),
    ("Who lead Soviet Union during it's collapse", 'mikhail gorbachev'),
    ('What did Michail Gorbachev do', 'introduced the liberalizing reforms of glasnost ( " openness " , c . 1985 ) and perestroika ( " reorganization " , 1987 ) and ended soviet involvement in afghanistan in 1989'),
    ('What liberalizing reforms by Michail Gorbachev', 'glasnost ( " openness " , c . 1985 ) and perestroika ( " reorganization " , 1987 )'),
    ('How did Soviet Union collapse', '1991 dissolution of the soviet union ( 26 december 1991 )'),
    # dalam Bahasa Indonesia
    ('Apa itu perang dingin', 'Periode ketegangan geopolitik antara Amerika Serikat dan Uni Soviet dan sekutu masing-masing, blok barat dan blok timur, yang dimulai setelah Perang Dunia II. Periode perang dingin umumnya dianggap mencakup doktrin truman 1947 (12 Maret 1947) hingga pembubaran Uni Soviet 1991 (26 Desember 1991). Istilah perang dingin digunakan karena tidak ada pertempuran skala besar langsung antara dua negara adidaya.'),
    ('Kapan perang dingin terjadi', 'Dimulai setelah Perang Dunia II'),
    ('Kapan perang dingin berakhir', '1991 pembubaran Uni Soviet (26 Desember 1991)'),
    ('Siapa yang terlibat dalam perang dingin', 'Amerika Serikat dan Uni Soviet'),
    ('Siapa memenangkan perang dingin', 'Amerika Serikat'),
    ('Apa itu bloc Barat', 'Blok barat dipimpin oleh Amerika Serikat.'),
    ('Apa itu bloc Timur', 'Blok timur dipimpin oleh Uni Soviet dan partai komunisnya.'),
    ('Apa yang Amerika lakukan masa perang dingin', 'Amerika Serikat ditinggalkan sebagai satu-satunya negara adidaya di dunia sebagai pemenang perang dingin.'),
    ('Apa yang Amerika lakukan masa perang dingin?', 'Membentuk Aliansi Militer NATO'),
    ('Apa yang Uni Soviet lakukan masa perang dingin?', 'Membentuk pakta Warsawa'),
    ('Apa itu NATO', 'Amerika Serikat dan sekutunya membentuk aliansi militer NATO.'),
    ('Apa itu pakta Warsawa', 'Uni Soviet membentuk pakta Warsawa pada tahun 1955 sebagai tanggapan terhadap NATO.'),
    ('Apa saja krisis besar dalam perang dingin', 'Tahun 1948 - Blokade Berlin, perang saudara Cina 1927- 1949, perang Korea 1950 -1953, revolusi Hongaria 1956, krisis suez 1956, krisis Berlin 1961 dan krisis rudal Kuba 1962.'),
    ('Kenapa istilah perang dingin dipakai', 'tidak ada pertempuran skala besar langsung antara dua negara adidaya'),
    ('Apa yang terjadi setelah krisis Kuba', 'Sino - perpecahan Soviet antara Cina dan Uni Soviet mempersulit hubungan dalam lingkup komunis'),
    ('Apa yang terjadi pada fase kedua perang dingin', 'perpecahan sino - soviet antara Cina dan Uni Soviet mempersulit hubungan dalam lingkup komunis'),
    ('Apa itu krisis Kuba', 'Krisis rudal Kuba 1962'),
    ('Siapa pemimpin Uni Soviet sewaktu bubar', 'mikhail gorbachev'),
    ('Apa yang Mikhail Gorbachev lakukan', 'memperkenalkan reformasi liberalisasi glasnost ("keterbukaan", c. 1985) dan perestroika ("reorganisasi", 1987)'),
    ('Bagaimana Uni Soviet bisa bubar?', 'Partai komunis uni soviet dilarang setelah upaya kudeta yang gagal.'),
]