india_wiki_text = """
India is the world's most populous democracy. A parliamentary republic with a multi-party system, it has eight recognised national parties, including the Indian National Congress and the Bharatiya Janata Party (BJP), and more than 40 regional parties. 
The Congress is considered centre-left in Indian political culture, and the BJP right-wing. 
For most of the period between 1950—when India first became a republic—and the late 1980s, the Congress held a majority in the parliament. 

In the Republic of India's first three general elections, in 1951, 1957, and 1962, the Jawaharlal Nehru-led Congress won easy victories. 
On Nehru's death in 1964, Lal Bahadur Shastri briefly became prime minister; he was succeeded, after his own unexpected death in 1966, by Nehru's daughter Indira Gandhi, 
who went on to lead the Congress to election victories in 1967 and 1971. Following public discontent with the state of emergency she declared in 1975, 
the Congress was voted out of power in 1977; the then-new Janata Party, which had opposed the emergency, was voted in. 
Its government lasted just over two years. Voted back into power in 1980, the Congress saw a change in leadership in 1984, when Indira Gandhi was assassinated; 
Indira Gandhi was succeeded by Indira Gandhi's son Rajiv Gandhi, who won an easy victory in the general elections later that year. 

A two-year period of political turmoil followed the general election of 1996. 

Narendra Damodardas Modi (born 17 September 1950) is an Indian politician serving as the 14th and current prime minister of India since 2014. 
Modi was the chief minister of Gujarat from 2001 to 2014 and is the Member of Parliament from Varanasi. 
Modi is a member of the Bharatiya Janata Party (BJP) and of the Rashtriya Swayamsevak Sangh (RSS), a right-wing Hindu nationalist paramilitary volunteer organisation. 

Ram Nath Kovind (born 1 October 1945) is an Indian politician serving as the 14th and current president of India since his inauguration in 2017.
"""

india_testing_dialog = [
    # in English
        ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about India', "india is the world ' s most populous democracy"),
    ('What kind of democracy is India?', 'parliamentary republic'),
    ('Who is first prime minister of India?', 'jawaharlal nehru'),
    ('How many times Jawaharlal Nehru won elections?', 'three'),
    ('Who is current president of India?', 'ram nath kovind'),
    ('Who is current prime minister of India?', 'narendra damodardas modi'),
    ('Who is Narendra Modi?', 'prime minister of india'),
    ('Which party is Narendra Modi?', 'bharatiya janata party'),
    ('Is BJP left wing?', 'right - wing'),
    ('Who was assassinated as prime minister in India?', 'indira gandhi'),
    ("Who was Indira Gandhi's son?", 'rajiv gandhi'),
    ('When was Modi born?', '17 september 1950'),
    ('What is dominant party in India?', 'indian national congress'),
    ('How many political party in India?', 'eight'),
    ('What party govern India now?', 'bharatiya janata party'),
    # dalam Bahasa Indonesia
    ('Apa bentuk demokrasi di India?', 'republik parlementer dengan sistem multi-partai'),
    ('siapa perdana menteri pertama di India?', 'jawaharlal nehru'),
    ('Berapa kali Jawaharlal Nehru menang pemilu?', 'Tiga'),
    ('Siapa presiden India sekarang?', 'ram nath kovind'),
    ('Siapa perdana menteri India sekarang?', 'narendra damodardas modi'),
    ('Siapa Narendra Modi?', 'Perdana Menteri India'),
    ('Apa partai politik Narendra Modi?', 'Partai Bharatiya Janata'),
    ('Apakah BJP sayap kiri?', 'kanan - sayap'),
    ('Siapa perdana menteri India yg dibunuh?', 'indira gandhi'),
    ('Siapa anak Indira Gandhi?', 'rajiv gandhi'),
    ('Ada berapa partai politik di India?', 'Delapan'),
    ('Partai apa yg dominan di India?', 'Kongres Nasional India'),
    ('Partai apa yang berkuasa di India sekarang?', 'Partai Bharatiya Janata'),
]
