indonesia_wiki_text = """
Indonesia is a presidential, constitutional republic with an elected legislature. 
Indonesia has 34 provinces, of which five have special status. The country's capital, Jakarta, is the world's second-most populous urban area.

Sukarno (born Koesno Sosrodihardjo; 6 June 1901 – 21 June 1970) was an Indonesian statesman, politician, nationalist, and assimilationist who was the first president of Indonesia, serving from 1945 to 1967.

Sukarno was the leader of the Indonesian struggle for independence from the Dutch colonialists. Upon Japanese surrender, Sukarno and Mohammad Hatta declared Indonesian independence on 17 August 1945, and Sukarno was appointed as its president. He led Indonesians in resisting Dutch re-colonisation efforts via diplomatic and military means until the Dutch recognition of Indonesian independence in 1949. Author Pramoedya Ananta Toer once wrote, "Sukarno was the only Asian leader of the modern era able to unify people of such differing ethnic, cultural and religious backgrounds without shedding a drop of blood."

After the events surrounding the 30 September Movement, the military general Suharto largely took control of the country and destroyed the PKI with executions of its members and sympathisers in several massacres, with support from the CIA and British intelligence services, resulting in an estimated 500,000 to over 1,000,000 deaths. In 1967, Suharto assumed the presidency, replacing Sukarno, who remained under house arrest until his death in 1970.

Joko Widodo (born 21 June 1961), also known as Jokowi, is an Indonesian politician and businessman who is the 7th and current president of Indonesia. Elected in July 2014, he was the first Indonesian president not to come from an elite political or military background.
"""

indonesia_testing_dialog = [
    # in English 
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Indonesia', 'indonesia is a presidential , constitutional republic with an elected legislature'),
    ("How Indonesia become independent", 'upon japanese surrender , sukarno and mohammad hatta declared indonesian independence on 17 august 1945'),
    ("who is first president of indonesia", 'sukarno'),
    ("who is Sukarno?", 'an indonesian statesman , politician , nationalist , and assimilationist'),
    ("when was Indonesia independent?", '17 august 1945'),
    ("When did Sukarno died?", '21 june 1970'),
    ("When Indonesia independence is recognized?", '1949'),
    ("Did Sukarno unify the country?", 'sukarno was the only asian leader of the modern era able to unify people of such differing ethnic , cultural and religious backgrounds without shedding a drop of blood'),
    ("Who colonized Indonesia?", 'dutch'),
    ("Who help Suharto to coup Sukarno?", 'cia and british intelligence services'),
    ("How many Suharto killed in Indonesia?", '500 , 000 to over 1 , 000 , 000'),
    ("who is current president of Indonesia?", 'joko widodo'),
    # dalam bahasa Indonesia
    ('bagaimana kemerdekaan bisa tercapai', 'Setelah Jepang menyerah, Soekarno dan Mohammad Hatta mendeklarasikan kemerdekaan Indonesia pada tanggal 17 Agustus 1945.'),
    ('siapa presiden pertama RI?', 'Sukarno'),
    ('siapa sukarno?', 'seorang negarawan Indonesia, politisi, nasionalis, dan asimilasi yang merupakan presiden pertama Indonesia'),
    ('kapan Indonesia menyatakan kemerdekaan?', '17 Agustus 1945'),
    ('kapan sukarno meninggal?', '21 Juni 1970'),
    ('kapan kemerdekaan Indonesia diakui?', '1949'),
    ('apakah Sukarno menyatukan Indonesia?', 'Sukarno adalah satu-satunya pemimpin Asia dari era modern yang mampu menyatukan orang-orang dari latar belakang etnis, budaya dan agama yang berbeda tanpa menumpahkan setetes darah.'),
    ('Siapa menjajah Indonesia?', 'Belanda'),
    ('Siapa membantu Suharto menggulingkan Sukarno?', 'Cia dan Dinas Intelijen Inggris'),
    ('Berapa banyak yang dibunuh Suharto?', '500, 000 hingga lebih dari 1.000 , 000'),
    ('Siapa presiden Indonesia sekarang?', 'joko widodo'),
    ('Siapa Joko Widodo?', 'Presiden indonesia ke-7 dan saat ini'),
]