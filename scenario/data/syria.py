syria_wiki_text = """
Syria, officially the Syrian Arab Republic, is a country in Western Asia. 
Syria is home to diverse ethnic and religious groups, including the majority Syrian Arabs, Kurds, Turkmens, Assyrians, Armenians, Circassians, Mandaeans, and Greeks.

Bashar Hafez al-Assad (born 11 September 1965) is a Syrian politician who is the 19th president of Syria, since 17 July 2000. 
In addition, he is the commander-in-chief of the Syrian Armed Forces and the Secretary-General of the Central Command of the Arab Socialist Ba'ath Party. 
His father, Hafez al-Assad, was the president of Syria before Bashar al-Assad, serving from 1971 to 2000.

Born and raised in Damascus, Bashar al-Assad graduated from the medical school of Damascus University in 1988 and began to work as a doctor in the Syrian Army. 
Four years later, he attended postgraduate studies at the Western Eye Hospital in London, specialising in ophthalmology.
In 1994, after his elder brother Bassel died in a car crash, Bashar was recalled to Syria to take over Bassel's role as heir apparent.

The unrest in Syria (which began on 15 March 2011 as part of the wider 2011 Arab Spring protests) grew out of discontent with the Syrian government and escalated to an armed conflict after protests calling for Assad's removal were violently suppressed.
The civil war is currently being fought by several factions, including the Syrian Armed Forces and its domestic and international allies, a loose alliance of mostly Sunni opposition rebel groups (such as the Free Syrian Army), Salafi jihadist groups (including al-Nusra Front and Tahrir al-Sham), the mixed Kurdish-Arab Syrian Democratic Forces (SDF), and the Islamic State of Iraq and the Levant (ISIL).
A number of foreign countries, such as Iran, Russia, Turkey, and the United States, have either directly involved themselves in the conflict or provided support to one or another faction in Syrian civil war. 
Iran, Russia, and Hezbollah support the Syrian Arab Republic and the Syrian Armed Forces militarily, 
with Russia conducting airstrikes and other military operations since September 2015.
"""

syria_testing_dialog = [
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Syria', 'syria , officially the syrian arab republic , is a country in western asia'),
    ('Who lives in Syria', 'syria is home to diverse ethnic and religious groups'),
    ('What ethnic lives in Syria', 'syria is home to diverse ethnic and religious groups , including the majority syrian arabs , kurds , turkmens , assyrians , armenians , circassians , mandaeans , and greeks'),
    ('Who is president of Syria', 'bashar hafez al - assad'),
    ('Who is Bashar al-assad', 'bashar hafez al - assad ( born 11 september 1965 ) is a syrian politician who is the 19th president of syria'),
    ('When Bashar al-assad become president', 'bashar hafez al - assad ( born 11 september 1965 ) is a syrian politician who is the 19th president of syria , since 17 july 2000 .'),
    ('Who is president before Bashar al-assad', 'hafez al - assad'),
    ('what caused syrian civil war', "protests calling for assad ' s removal were violently suppressed"),
    ('Who was involved in Syrian civil war', 'a number of foreign countries , such as iran , russia , turkey , and the united states'),
    ('What caused the unrest in Syria', "discontent with the syrian government and escalated to an armed conflict after protests calling for assad ' s removal were violently suppressed"),
    ('What was syrian civil war part of', 'arab spring protests'),
    ('What was Russia role in Syrian civil war', 'russia conducting airstrikes and other military operations'),
    ('What was Bashar al-assad work before becoming president', 'doctor in the syrian army'),
    ('What was Bashar al-assad education?', 'medical school of damascus university'),
    ('What protest tried to bring down Bashar al-assad', 'arab spring protests'),
    ('When was arab spring unrest in Syria', '15 march 2011'),
    # dalam Bahasa Indonesia
    ('Siapa yang tinggal di Suriah', 'Suriah adalah rumah bagi beragam kelompok etnis dan agama.'),
    ('Etnis apa saja yang tinggal di Suriah', 'arab Suriah , Kurdi, Turkmens , asyur, Armenia , circassians , mandaeans, dan Yunani'),
    ('Siapa presiden Suriah sekarang', 'bashar hafez al - assad'),
    ('Siapa Bashar al-assad', 'Bashar Hafez al - Assad (lahir 11 September 1965) adalah seorang politikus Suriah yang merupakan presiden ke-19 Suriah.'),
    ('Kapan Bashar al-assad menjadi presiden', '17 Juli 2000'),
    ('Siapa presiden sebelum Bashar al-assad', 'hafez al - assad'),
    ('Siapa saja yang terlibat perang saudara di Suriah', 'sejumlah negara asing, seperti Iran, Rusia, Turki, dan Amerika Serikat'),
    ('Kapan perang saudara Suriah terjadi', '15 Maret 2011'),
    ('Apa penyebab kekacauan di Suriah', 'Ketidakpuasan dengan pemerintah Suriah dan meningkat menjadi konflik bersenjata setelah protes yang menyerukan penghapusan Assad ditekan dengan keras.'),
    ('Perang saudara Suriah itu disebabkan oleh apa', 'Ketidakpuasan terhadap pemerintah Suriah'),
    ('Apa penyebab perang saudara di Suriah', 'Ketidakpuasan dengan pemerintah Suriah dan meningkat menjadi konflik bersenjata setelah protes yang menyerukan penghapusan Assad ditekan dengan keras.'),
    ('Apa kontribusi Russia dalam perang saudara Suriah', 'Rusia melakukan serangan udara dan operasi militer lainnya'),
    ('Sebelum menjadi presiden, apa pekerjaan Bashar al-assad', 'dokter'),
    ('Apa pendidikan Bashar al-assad', 'Bashar al - assad lulus dari sekolah kedokteran universitas Damaskus'),
    ('Protes apa yang mencoba menjatuhkan Bashar al-assad', 'Protes musim semi Arab 2011'),
    ('Kapan kekacauan musim semi Arab terjadi di Suriah', '15 Maret 2011'),
]