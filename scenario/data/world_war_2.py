world_war_2_wiki_text = """
World War II or the Second World War, often abbreviated as WWII or WW2, was a global war that lasted from 1939 to 1945. 
It involved the vast majority of the world's countries—including all of the great powers—forming two opposing military alliances: the Allies and the Axis powers. 
In a total war directly involving more than 100 million personnel from more than 30 countries, the major participants threw their entire economic, industrial, and scientific capabilities behind the war effort, blurring the distinction between civilian and military resources. 
Aircraft played a major role in the conflict, enabling the strategic bombing of population centres and the only two uses of nuclear weapons in war. 
World War II was by far the deadliest conflict in human history; it resulted in 70 to 85 million fatalities, a majority being civilians. 
Tens of millions of people died due to genocides (including the Holocaust), starvation, massacres, and disease. 
In the wake of the Axis defeat, Germany and Japan were occupied, and war crimes tribunals were conducted against German and Japanese leaders.

The war in Europe concluded with the liberation of German-occupied territories, and the invasion of Germany by the Western Allies and the Soviet Union, 
culminating in the fall of Berlin to Soviet troops, Hitler's suicide and the German unconditional surrender on 8 May 1945. 

United States dropped the first atomic bombs on the Japanese cities of Hiroshima, on 6 August, and Nagasaki, on 9 August. 
Faced with an imminent invasion of the Japanese archipelago, the possibility of additional atomic bombings, 
and the Soviet entry into the war against Japan and its invasion of Manchuria, 
Japan announced its intention to surrender on 15 August, then signed the surrender document on 2 September 1945, 
cementing total victory in Asia for the Allies.
"""


world_war_2_testing_dialog = [
    # in English
    ('Tell me about wars', '''Welcome to wars subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology

Or move subtopic to specific wars (world war 1, world war 2, cold war):
Tell me about cold war

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about WW 2', 'world war ii'),
    ('Who were involved in WW 2', "it involved the vast majority of the world ' s countries — including all of the great powers — forming two opposing military alliances : the allies and the axis powers"),
    ('Who won the world war 2', 'the allies'),
    ('Who won the WW 2', 'the allies .'),
    ('How the world war 2 ended', "the war in europe concluded with the liberation of german - occupied territories , and the invasion of germany by the western allies and the soviet union , culminating in the fall of berlin to soviet troops , hitler ' s suicide and the german unconditional surrender on 8 may 1945"),
    ('How the japanese surrender', 'japan announced its intention to surrender on 15 august , then signed the surrender document on 2 september 1945'),
    ('What caused Japanese to surrender', "Sorry, I don't know about that."),
    ('What cause japanese to surrender', 'an imminent invasion of the japanese archipelago , the possibility of additional atomic bombings , and the soviet entry into the war against japan and its invasion of manchuria'),
    ('How many casualties in WW 2', '70 to 85 million'),
    ('What new weapon was used in WW 2', 'nuclear weapons'),
    ('Which country dropped the nuclear bomb', 'united states'),
    ('Who was the victim of nuclear weapon', 'world war ii was by far the deadliest conflict in human history ; it resulted in 70 to 85 million fatalities , a majority being civilians'),
    ('Where was the nuclear bomb dropped', 'japanese cities of hiroshima , on 6 august , and nagasaki , on 9 august'),
    ('Who were the victim of WW 2', '70 to 85 million fatalities , a majority being civilians'),
    # dalam Bahasa Indonesia
    ('Ceritakan tentang perang dunia kedua', 'Perang Dunia II'),
    ('Siapa memenangkan PD 2', 'Sekutu'),
    ('Bagaimana PD 2 berakhir', 'Perang di Eropa diakhiri dengan pembebasan wilayah yang diduduki Jerman, dan invasi Jerman oleh sekutu barat dan Uni Soviet, yang berpuncak pada jatuhnya Berlin ke pasukan Soviet, bunuh diri Hitler dan penyerahan tanpa syarat Jerman pada tanggal 8 Mei 1945.'),
    ('Bagaimana Jepang menyerah', 'Jepang mengumumkan niatnya untuk menyerah pada tanggal 15 Agustus 1945, kemudian menandatangani dokumen penyerahan pada tanggal 2 September 1945.'),
    ('Apa yang menyebabkan Jepang menyerah', 'Maaf, saya tidak tahu tentang itu.'),
    ('Kenapa Jepang menyerah', 'dihadapkan dengan invasi segera ke kepulauan Jepang, kemungkinan pemboman atom tambahan, dan masuknya Soviet ke dalam perang melawan Jepang dan invasinya ke Manchuria.'),
    ('Berapa banyak korban perang dunia 2', '70 hingga 85 juta'),
    ('Apa senjata baru dalam perang dunia 2', 'senjata nuklir'),
    ('Siapa korban senjata nuklir', 'Warga sipil'),
    ('Di mana dijatuhkan bom nuklir', 'kota Hiroshima di Jepang, pada tanggal 6 Agustus , dan Nagasaki, pada tanggal 9 Agustus'),
    ('Siapa korban perang dunia 2', 'Perang dunia II sejauh ini merupakan konflik paling mematikan dalam sejarah manusia; Itu mengakibatkan 70 hingga 85 juta kematian, mayoritas adalah warga sipil.'),
    ('Where was nuclear bomb dropped', 'united states dropped the first atomic bombs on the japanese cities of hiroshima , on 6 august , and nagasaki , on 9 august'),
]