politics_wiki_text = """
Politics is the set of activities that are associated with making decisions in groups, or other forms of power relations between individuals, such as the distribution of resources or status. 

Forms of government can be classified by several ways. 
Forms of government by the structure of power are monarchies (including constitutional monarchies) and republics (usually presidential, semi-presidential, or parliamentary).
A monarchy is a form of government in which a person, the monarch, is head of state for life or until abdication.
A republic (from Latin res publica 'public affair') is a form of government in which "supreme power is held by the people and their elected representatives".

Forms of government by separation of powers describes the degree of horizontal integration between the legislature, the executive, the judiciary, and other independent institutions.

Source of power of government differs between democracies, oligarchies, and autocracies.

Democracy is a form of government in which the people have the authority to deliberate and decide legislation ("direct democracy"), 
or to choose governing officials to do so ("representative democracy"). 
In a democracy, political legitimacy is based on popular sovereignty. 
Forms of democracy include representative democracy, direct democracy, and demarchy. 
These are separated by the way decisions are made, whether by elected representatives, referendums, or by citizen juries. 
Democracies can be either republics or constitutional monarchies.

Oligarchy is a power structure where a minority rules. 
Oligarchy may be in the form of anocracy, aristocracy, ergatocracy, geniocracy, gerontocracy, kakistocracy, kleptocracy, meritocracy, noocracy, particracy, plutocracy, stratocracy, technocracy, theocracy, or timocracy.

Autocracies are either dictatorships (including military dictatorships) or absolute monarchies.
Constitutions are written documents that specify and limit the powers of the different branches of government. 

A form of government that is built on corruption is called a kleptocracy ('rule of thieves').
"""

politics_testing_dialog = [
    ('Tell me about terminologies', '''Welcome to terminologies subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about politics', 'politics is the set of activities that are associated with making decisions in groups , or other forms of power relations between individuals'),
    ('what are form of politics', 'politics is the set of activities that are associated with making decisions in groups , or other forms of power relations between individuals'),
    ('what are types of politics', 'politics is the set of activities that are associated with making decisions in groups , or other forms of power relations between individuals'),
    ('what are forms of government', 'forms of government by the structure of power are monarchies ( including constitutional monarchies ) and republics ( usually presidential , semi - presidential , or parliamentary )'),
    ('what are source of power for government', 'source of power of government differs between democracies , oligarchies , and autocracies'),
    ('what is autocracies', 'either dictatorships ( including military dictatorships ) or absolute monarchies'),
    ('what is oligarchies', 'a power structure where a minority rules'),
    ('what is democracy', 'a form of government in which the people have the authority to deliberate and decide legislation ( " direct democracy " ) , or to choose governing officials to do so'),
    ('what is rules of thiefs', 'kleptocracy'),
    ('what is kleptocracy', 'a form of government that is built on corruption'),
    ('what are corrupt government', 'kleptocracy'),
    ('ceritakan tentang politik', 'Serangkaian kegiatan yang terkait dengan membuat keputusan dalam kelompok, atau bentuk lain dari hubungan kekuasaan antara individu'),
    ('ada bentuk pemerintahan apa saja', 'Republik'),
    ('apa semua bentuk pemerintahan', 'Bentuk pemerintahan dengan struktur kekuasaan adalah monarki (termasuk monarki konstitusional) dan republik (biasanya presiden, semi-presiden, atau parlementer)'),
    ('apa sumber kekuasaan pemerintah', 'Sumber kekuasaan pemerintah'),
    ('apa jenis sumber kekuasaan', 'Sumber kekuasaan pemerintah berbeda antara demokrasi, oligarki, dan otokrasi'),
    ('apa itu otokrasi', 'Otokrasi adalah kediktatoran (termasuk kediktatoran militer) atau monarki absolut.'),
    ('apa itu oligarki', 'Struktur kekuasaan di mana aturan minoritas'),
    ('apa itu demokrasi', 'Suatu bentuk pemerintahan di mana rakyat memiliki wewenang untuk mempertimbangkan dan memutuskan undang-undang ("demokrasi langsung"), atau memilih pejabat yang memerintah untuk melakukannya'),
    ('apa pemerintahan yang korupsi', 'kleptokrasi'),
    ('apa itu kleptokrasi', 'Bentuk pemerintahan yang dibangun di atas korupsi'),
]