iran_wiki_text = """
Iran is an Islamic theocracy which includes elements of a presidential democracy, with the ultimate authority vested in an autocratic "Supreme Leader", a position held by Ali Khamenei since Khomeini's death in 1989.

Sayyid Ruhollah Musavi Khomeini (17 May 1900 – 3 June 1989), also known as Ayatollah Khomeini, was an Iranian political and religious leader. 
He was the founder of the Islamic Republic of Iran and the leader of the 1979 Iranian Revolution, which saw the overthrow of the last Shah of Iran, Mohammad Reza Pahlavi, and the end of the Persian monarchy. 
Following the revolution, Khomeini became the country's first supreme leader, a position created in the constitution of the Islamic Republic as the highest-ranking political and religious authority of the nation, which he held until his death. 
Most of Khomeini period in power was taken up by the Iran–Iraq War of 1980–1988. He was succeeded by Ali Khamenei on 4 June 1989.

Sayyid Ali Hosseini Khamenei (born 19 April 1939) is a Twelver Shia Marja' and the second and current supreme leader of Iran, in office since 1989. 
He was previously the third president of Iran from 1981 to 1989. 
Khamenei is the longest serving head of state in the Middle East, as well as the second-longest serving Iranian leader of the last century, after Shah Mohammad Reza Pahlavi.

According to his official website, Khamenei was arrested six times before being sent into exile for three years during Mohammad Reza Pahlavi's reign. 
After the Iranian revolution overthrowing the shah, he was the target of an attempted assassination in June 1981 that paralysed his right arm.
The Revolutionary Guards have been deployed to suppress opposition to Ali Khamenei. 

Supreme Leader is the head of state of Iran, the commander-in-chief of its armed forces, 
and can issue decrees and make the final decisions on the main policies of the government in many fields such as economy, 
the environment, foreign policy, and national planning in Iran.

The current president of Iran is Ebrahim Raisi, who assumed office on 3 August 2021, after the 2021 presidential election. 
He succeeded Hassan Rouhani, who served 8 years in office from 2013 to 2021.
"""

iran_testing_dialog = [
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Iran', 'iran is an islamic theocracy'),
    ('What kind of government is Iran?', 'islamic theocracy'),
    ('What is the leader of Iran called? ', 'supreme leader'),
    ('Who was last king of Iran?', 'mohammad reza pahlavi'),
    ('Who is Reza Pahlavi? ', 'shah of iran'),
    ('Who was the first supreme leader of Iran?', 'sayyid ruhollah musavi khomeini'),
    ('What did Khomeini do? ', 'leader of the 1979 iranian revolution'),
    ('What is Khomeini called? ', 'ayatollah khomeini'),
    ('Who fight war with Iran?', 'iran – iraq war'),
    ('When was Iran - Iraq war?', '1980 – 1988'),
    ('When was Ayatollah Khomeini died?', '3 june 1989'),
    ('Who replaced Ayatollah Khomeini? ', 'ali khamenei'),
    ('Who is Ali Khamenei?', "supreme leader"),
    ('Who is current supreme leader of Iran?', 'ali khamenei'),
    ('Was Khamenei assassinated?', 'an attempted assassination in june 1981 that paralysed his right arm'),
    ('What is supreme leader in Iran?', 'head of state of iran'),
    ('What is the power of supreme leader in Iran?', 'head of state of iran'),
    ('What could supreme leader in Iran do?', 'issue decrees and make the final decisions on the main policies of the government'),
    ('Who is the current president of Iran?', 'ebrahim raisi'),
    ('When Raisi become president of Iran?', '3 august 2021'),
    # dalam Bahasa Indonesia
    ('Apa bentuk pemerintahan di Iran?', 'Teokrasi Islam'),
    ('Siapa raja terakhir di Iran?', 'mohammad reza pahlavi'),
    ('Siapa Mohammad Reza Pahlavi?', 'Shah dari Iran'),
    ('Apa sebutan pemimpin di Iran?', 'pemimpin tertinggi'),
    ('Siapa pemimpin tertinggi pertama di Iran?', 'sayyid ruhollah musavi khomeini'),
    ('Apa yang Khomeini lakukan?', 'Pemimpin Revolusi Iran 1979'),
    ('Apa sebutan untuk Khomeini?', 'ayatullah'),
    ('Siapa yang berperang dengan Iran?', 'Iran – Perang Irak'),
    ('kapan perang iran - irak?', '1980 – 1988'),
    ('kapan Ayatollah Khomeini meninggal?', '3 Juni 1989'),
    ('siapa menggantikan Khomeini?', 'ali khamenei'),
    ('Siapakah Ali Khamenei itu?', "pemimpin tertinggi"),
    ('Apakah Khamenei pernah dicoba dibunuh?', 'Dia adalah target percobaan pembunuhan pada juni 1981 yang melumpuhkan lengan kanannya.'),
    ('Apakah Khamenei dibunuh?', 'percobaan pembunuhan pada juni 1981 yang melumpuhkan lengan kanannya'),
    ('Apa yang dapat dilakukan pemimpin tertinggi di Iran?', 'mengeluarkan keputusan dan membuat keputusan akhir tentang kebijakan utama pemerintah'),
    ('Siapa presiden Iran sekarang?', 'ebrahim raisi'),
    ('Kapan Ebrahim Raisi menjadi presiden?', '3 Agustus 2021'),
    ('Siapa yang digantikan Raisi?', 'hassan rouhani'),
]