from run_interactive import turn_handler, actor
from fastapi import FastAPI, WebSocket
from fastapi.responses import HTMLResponse

app = FastAPI()

html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <style>
        input {
            display: block;
            width: 28em;
            margin-left: 2em;
            margin-bottom: 10em;
            float: left;
        }
        button {
            display: block;
            width: 5em;
            margin-left: 1em;
            float: left;
        }
        p {
            background-color: #f2f2f2;
        }
    </style>
    <body>
        <h1>Politics Chat</h1>
        <h3>Bilingual: English and Indonesian</h3>
        <ul id='messages'>
        </ul>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <script>
            var ws = new WebSocket("ws://localhost:8000/ws");
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                if ((event.data).startsWith('Answer')) {
                    message = document.createElement("p")
                } 
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""

@app.get("/")
async def get():
    return HTMLResponse(html)

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    ctx = {}
    await websocket.accept()
    while True:
        question = await websocket.receive_text()
        await websocket.send_text(f"Question: {question}")
        answer, ctx = turn_handler(question, ctx, actor)
        await websocket.send_text(f"Answer: {answer}")

# @app.get("/api_query")
# async def api_query(q: str):
#     question = q
#     answer = run_query(question)
#     return {"question" : question, 
#             "answer": answer}
