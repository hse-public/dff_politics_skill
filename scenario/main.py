import re
from typing import Any

from df_engine.core import Context, Actor
from df_engine.core.keywords import GLOBAL, TRANSITIONS, RESPONSE
import df_engine.conditions as cnd
import df_engine.labels as labels

import scenario.condition as local_cond 
from scenario.response import *

# create plot of dialog
# plot = {
#     GLOBAL: {TRANSITIONS: {("flow", "node_hi"): cnd.exact_match("Hi"), 
#                             ("flow", "node_topic"): cnd.regexp(r'\w+')}},
#     "flow": {
#         "node_hi": {RESPONSE: "Hi!!!"},
#         "node_topic": {RESPONSE: topic_response},
#     },
# }

plot = {
    "global": {
        "start": {
            RESPONSE: bot_intro,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("politicians", "start"): local_cond.ask_politicians,
                labels.repeat(): cnd.true() # nothing match go loop until something matches
            }
        },
        "fallback": { # an error occured while agent was running
            RESPONSE: "Sorry, something went wrong. We will restart",
            TRANSITIONS: {
                ("global", "start"): cnd.true()
            }
        }
    },
    "terminologies": {
        "start": {
            RESPONSE: terminologies_intro,
            TRANSITIONS: {
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("terminologies", "politics"): local_cond.ask_politics,
                ("terminologies", "democracy"): local_cond.ask_democracy,
                ("terminologies", "monarchy"): local_cond.ask_monarchy,
                ("terminologies", "republics"): local_cond.ask_republics,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }
        },
        "politics": {
            RESPONSE: politics_topic_response,
            TRANSITIONS: {
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("terminologies", "politics"): local_cond.ask_politics,
                ("terminologies", "democracy"): local_cond.ask_democracy,
                ("terminologies", "monarchy"): local_cond.ask_monarchy,
                ("terminologies", "republics"): local_cond.ask_republics,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }

        },
        "democracy": {
            RESPONSE: democracy_topic_response,
            TRANSITIONS: {
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("terminologies", "politics"): local_cond.ask_politics,
                ("terminologies", "democracy"): local_cond.ask_democracy,
                ("terminologies", "monarchy"): local_cond.ask_monarchy,
                ("terminologies", "republics"): local_cond.ask_republics,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }

        },
        "monarchy": {
            RESPONSE: monarchy_topic_response,
            TRANSITIONS: {
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("terminologies", "politics"): local_cond.ask_politics,
                ("terminologies", "democracy"): local_cond.ask_democracy,
                ("terminologies", "monarchy"): local_cond.ask_monarchy,
                ("terminologies", "republics"): local_cond.ask_republics,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }

        },
        "republics": {
            RESPONSE: republics_topic_response,
            TRANSITIONS: {
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("terminologies", "politics"): local_cond.ask_politics,
                ("terminologies", "democracy"): local_cond.ask_democracy,
                ("terminologies", "monarchy"): local_cond.ask_monarchy,
                ("terminologies", "republics"): local_cond.ask_republics,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }
        }
    }, 
    "countries": {
        "start": {
            RESPONSE: countries_intro,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "china": {
            RESPONSE: china_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        }, 
        "egypt": {
            RESPONSE: egypt_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "france": {
            RESPONSE: france_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "germany": {
            RESPONSE: germany_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "india": {
            RESPONSE: india_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "indonesia": {
            RESPONSE: indonesia_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "iran": {
            RESPONSE: iran_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "libya": {
            RESPONSE: libya_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        }, 
        "russia": {
            RESPONSE: russia_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        }, 
        "syria": {
            RESPONSE: syria_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        },
        "usa": {
            RESPONSE: usa_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "china"): local_cond.ask_china,
                ("countries", "egypt"): local_cond.ask_egypt,
                ("countries", "france"): local_cond.ask_france,
                ("countries", "germany"): local_cond.ask_germany,
                ("countries", "india"): local_cond.ask_india,
                ("countries", "indonesia"): local_cond.ask_indonesia,
                ("countries", "iran"): local_cond.ask_iran,
                ("countries", "libya"): local_cond.ask_libya,
                ("countries", "russia"): local_cond.ask_russia,
                ("countries", "syria"): local_cond.ask_syria,
                ("countries", "usa"): local_cond.ask_usa,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                labels.repeat(): cnd.true() # nothing match go loop until something matches     
            }
        }
    },
    "wars": {
        "start": {
            RESPONSE: wars_intro,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "world_war_1"): local_cond.ask_ww1,
                ("wars", "world_war_2"): local_cond.ask_ww2,
                ("wars", "cold_war"): local_cond.ask_cold_war,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }            
        }, 
        "world_war_1": {
            RESPONSE: ww1_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("wars", "world_war_1"): local_cond.ask_ww1,
                ("wars", "world_war_2"): local_cond.ask_ww2,
                ("wars", "cold_war"): local_cond.ask_cold_war,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }            
        },
        "world_war_2": {
            RESPONSE: ww2_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("wars", "world_war_1"): local_cond.ask_ww1,
                ("wars", "world_war_2"): local_cond.ask_ww2,
                ("wars", "cold_war"): local_cond.ask_cold_war,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }            
        }, 
        "cold_war": {
            RESPONSE: cold_war_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("politicians", "start"): local_cond.ask_politicians,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("wars", "world_war_1"): local_cond.ask_ww1,
                ("wars", "world_war_2"): local_cond.ask_ww2,
                ("wars", "cold_war"): local_cond.ask_cold_war,
                ("global", "start"): local_cond.ask_for_restart,
                ("wars", "start"): local_cond.ask_wars,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }            
        },
    }, 
    "politicians": {
        "start": {
            RESPONSE: politicians_intro,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("global", "start"): local_cond.ask_for_restart,
                ("politicians", "Putin"): local_cond.ask_putin,
                ("politicians", "Xi"): local_cond.ask_xi,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }
        },
        "Putin": {
            RESPONSE: putin_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("global", "start"): local_cond.ask_for_restart,
                ("politicians", "Putin"): local_cond.ask_putin,
                ("politicians", "Xi"): local_cond.ask_xi,
                ("politicians", "start"): local_cond.ask_politicians,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }
        }, 
        "Xi": {
            RESPONSE: xi_topic_response,
            TRANSITIONS: {
                ("terminologies", "start"): local_cond.ask_terminologies,
                ("wars", "start"): local_cond.ask_wars,
                ("countries", "start"): local_cond.ask_countries,
                ("wars", "start"): local_cond.ask_wars,
                ("global", "start"): local_cond.ask_for_restart,
                ("politicians", "Putin"): local_cond.ask_putin,
                ("politicians", "Xi"): local_cond.ask_xi,
                ("politicians", "start"): local_cond.ask_politicians,
                labels.repeat(): cnd.true() # nothing match go loop until something match
            }
        }, 
    }
}


# init actor
actor = Actor(plot, start_label=("global", "start"))


# TODO: extend graph
# plot = {
#     "service": {
#         "start": {RESPONSE: ""},
#         "fallback": {RESPONSE: "hi"},
#     },
# }

# actor = Actor(plot, start_label=("service", "start"), 
#                     fallback_label=("service", "fallback"))
