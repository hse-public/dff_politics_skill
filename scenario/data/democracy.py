democracy_wiki_text = """
Democracy is a form of government in which the people have the authority to deliberate and decide legislation ("direct democracy"), 
or to choose governing officials to do so ("representative democracy"). 
Who is considered part of "the people" and how authority is shared among or delegated by the people has changed over time and at different rates in different countries, 
but over time more and more of a democratic country's inhabitants have generally been included. 
Cornerstones of democracy include freedom of assembly, association and speech, inclusiveness and equality, citizenship, consent of the governed, voting rights, freedom from unwarranted governmental deprivation of the right to life and liberty, 
and minority rights.

There are two forms of democracy, direct democracy and representative democracy. 
The original form of democracy was a direct democracy. 
The most common form of democracy today is a representative democracy, where the people elect government officials to govern on their behalf such as in a parliamentary or presidential democracy.[2]

Prevalent day-to-day decision making of democracies is the majority rule, 
though other decision making approaches like supermajority and consensus have also been integral to democracies. 
They serve the crucial purpose of inclusiveness and broader legitimacy on sensitive issues—counterbalancing majoritarianism—and therefore mostly take precedence on a constitutional level. 
In the common variant of liberal democracy, the powers of the majority are exercised within the framework of a representative democracy, 
but the constitution limits the majority and protects the minority—usually through the enjoyment by all of certain individual rights, 
e.g. freedom of speech or freedom of association.

Democracy contrasts with forms of government where power is either held by an individual, as in autocratic systems like absolute monarchy, or where power is held by a small number of individuals, as in an oligarchy—oppositions inherited from ancient Greek philosophy. 
Karl Popper defined democracy in contrast to dictatorship or tyranny, focusing on opportunities for the people to control their leaders and to oust them without the need for a revolution. 
World public opinion strongly favors democratic systems of government.
"""

democracy_testing_dialog = [
    ('Tell me about terminologies', '''Welcome to terminologies subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about democracy', 'democracy is a form of government in which the people have the authority to deliberate and decide legislation ( " direct democracy " ) , or to choose governing officials to do so ( " representative democracy " )'),
    ('what is the most common democracy today', 'representative democracy'),
    ('what is the opposite of democracy', 'dictatorship or tyranny'),
    ('apa itu demokrasi', 'suatu bentuk pemerintahan di mana rakyat memiliki wewenang untuk mempertimbangkan dan memutuskan undang-undang ("demokrasi langsung"), atau memilih pejabat yang memerintah untuk melakukannya ("demokrasi perwakilan")'),
    ('apa bentuk demokrasi paling umum', 'demokrasi perwakilan'),
    ('apa kebalikan dari demokrasi', 'kediktatoran atau tirani'),
]