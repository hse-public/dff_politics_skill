xi_wiki_text = """
Xi Jinping was born in Beijing on 15 June 1953, the second son of Xi Zhongxun and his wife Qi Xin. 
Xi was elected President of the People's Republic of China on 14 March 2013, in a confirmation vote by the 12th National People's Congress in Beijing. 

Xi vowed to crack down on corruption almost immediately after he ascended to power at the 18th Party Congress. 
In his inaugural speech as general secretary, Xi mentioned that fighting corruption was one of the toughest challenges for the party. 
Xi also vowed to root out "tigers and flies", that is, corrupt high-ranking officials and ordinary party functionaries.
The misfortunes and suffering of his family in his early years hardened Xi's view of politics. 
During an interview in 2000, Xi said, "People who have little contact with power, who are far from it, 
always see these things as mysterious and novel. 
But what I see is not just the superficial things: the power, the flowers, the glory, the applause. 
I see the bullpens and how people can blow hot and cold. I understand politics on a deeper level." 

From 1973, Xi applied to join the Chinese Communist Party ten times and was finally accepted on his tenth attempt in 1974.

A Chinese nationalist, Xi has reportedly taken a hard-line on security issues as well as foreign affairs, projecting a more nationalistic and assertive China on the world stage. His political program calls for a China more united and confident of its own value system and political structure.

The Belt and Road Initiative (BRI) has been called Xi's "signature project" since 2013, involving numerous infrastructure development and investment projects throughout Asia, Europe, Africa, and the Americas. 

In September 2020 Xi Jinping announced that China will "strengthen its 2030 climate target (NDC), peak emissions before 2030 and aim to achieve carbon neutrality before 2060.

Xi Jinping is widely popular in China. 

In 2017, The Economist named Xi the most powerful person in the world. In 2018, Forbes ranked Xi as the most powerful and influential person in the world, replacing Russian President Vladimir Putin who had been ranked so for five consecutive years.
"""


xi_testing_dialog = [
    # about Xi Jinping
    ("Tell me about Politicians", '''Welcome to politicians subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology
Tell me about wars

Or move subtopic to specific politician (Putin, Xi Jinping):
Tell me about Putin

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Xi', 'xi jinping'),
    ("who leads China?", 'xi jinping'),
    ('what is capital of China?', 'beijing'),
    ("who is president of China?", 'xi jinping'),
    ("who leads CPC?", 'xi jinping'),
    ('What is CPC?', 'chinese communist party'),
    ("how is Xi foreign policy", 'projecting a more nationalistic and assertive china on the world stage'),
    ("did Xi try to join CPC", 'xi applied to join the chinese communist party ten times and was finally accepted on his tenth attempt in 1974'),
    ("when Xi become president of china", '2013'),
    ("when did Xi born", '15 june 1953'),
    ("what is Xi climate policy", 'china will " strengthen its 2030 climate target ( ndc ) , peak emissions before 2030 and aim to achieve carbon neutrality before 2060'),
    ("what is Xi project", 'the belt and road initiative'),
    ("who involve in BRI", 'numerous infrastructure development and investment projects throughout asia , europe , africa , and the americas'),
    ("what is belt and road initiative", 'the belt and road initiative ( bri ) has been called xi \' s " signature project " since 2013 , involving numerous infrastructure development and investment projects throughout asia , europe , africa , and the americas'),
    ("what Xi fight for", 'crack down on corruption'),
    ('why Xi crack down on corruption', 'fighting corruption was one of the toughest challenges for the party'),
    ("what Xi said in interview", 'during an interview in 2000 , xi said , " people who have little contact with power , who are far from it , always see these things as mysterious and novel . but what i see is not just the superficial things : the power , the flowers , the glory , the applause . i see the bullpens and how people can blow hot and cold . i understand politics on a deeper level . "'),
    ('Has Xi suffer?', 'the misfortunes and suffering of his family in his early years'), 
    ('Is Xi going to attack Taiwan?', "Sorry, I don't know about that."),
    # tentang Xi Jinping 
    ('siapa pemimpin Cina', 'xi jinping'),
    ('Apa ibukota Tiongkok?', 'Beijing'),
    ('siapa presiden Cina', 'xi jinping'),
    ('siapa pemimpin CPC', 'xi jinping'),
    ('Apa itu CPC?', 'Partai Komunis Tiongkok'),
    ('bagaimana kebijakan luar negeri Xi', 'memproyeksikan Cina yang lebih nasionalis dan tegas di panggung dunia'),
    ('apakah Xi bergabung dengan PKT?', "Xi mengajukan permohonan untuk bergabung dengan partai komunis Cina sepuluh kali dan akhirnya diterima pada upaya kesepuluh pada tahun 1974."),
    ('kapan Xi menjadi presiden Cina', '14 Maret 2013'),
    ('kapan Xi lahir', '15 Juni 1953'),
    ('apa kebijakan iklim Xi?', 'memperkuat target iklim 2030'),
    ('apa proyek Xi?', 'Inisiatif sabuk dan jalan'),
    ('apa itu initiatif sabuk dan jalan?','berbagai proyek pembangunan infrastruktur dan investasi di seluruh Asia, Eropa, Afrika, dan Amerika'),
    ('apa tantangan Xi?', 'memerangi korupsi'),
    ('kenapa Xi memerangi korupsi', "Memerangi korupsi adalah salah satu tantangan terberat bagi partai."),
    ('apa yang Xi bilang dalam wawancara?','Orang-orang yang memiliki sedikit kontak dengan kekuasaan, yang jauh dari itu, selalu melihat hal-hal ini sebagai misterius dan baru. Tetapi apa yang saya lihat bukan hanya hal-hal dangkal: kekuatan, bunga, kemuliaan, tepuk tangan. Saya melihat bullpens dan bagaimana orang bisa meniup panas dan dingin. Saya memahami politik pada tingkat yang lebih dalam. "'),
    ('apakah Xi pernah menderita?', "Kemalangan dan penderitaan keluarganya di tahun-tahun awal"),
    ('Apakah Xi akan menyerang Taiwan?', 'Maaf, saya tidak tahu tentang itu.'),
]