putin_wiki_text = """
Vladimir Vladimirovich Putin, born 7 October 1952, is a Russian politician and former intelligence officer who is serving as the current president of Russia. 
He has been serving in this position since 2012, and he previously held this office from 1999 until 2008. 
He was also the prime minister from 1999 to 2000, and again from 2008 to 2012. 

Putin was born in Leningrad (now Saint Petersburg) and studied law at Leningrad State University, graduating in 1975. 
Putin worked as a KGB foreign intelligence officer for 16 years, rising to the rank of lieutenant colonel, before resigning in 1991 to begin a political career in Saint Petersburg. 
He later moved to Moscow in 1996 to join the administration of president Boris Yeltsin. 
After the resignation of Yeltsin, Putin became acting president, and less than four months later was elected outright to his first term as president and was reelected in 2004. 
And was reelected as president in 2012, and again in 2018. 

During his first tenure as president, the Russian economy grew for eight straight years, with GDP measured by purchasing power increasing by 72%, 
real incomes increased by a factor of 2.5, real wages more than tripled; 
unemployment and poverty more than halved and the Russians' self-assessed life satisfaction rose significantly. 
The growth was a result of a fivefold increase in the price of oil and gas, which constitute the majority of Russian exports, 
recovery from the post-Communist depression and financial crises, a rise in foreign investment, and prudent economic and fiscal policies. 
Internationally, Putin led Russia to victory in the Second Chechen War and the Russo-Georgian War. 
Serving under Dmitry Medvedev from 2008 to 2012, he oversaw large-scale military reform and police reform. 
Putin received 76% of the vote in the 2018 election and was re-elected for a six-year term ending in 2024.

Putin has promoted explicitly conservative policies in social, cultural, and political matters, both at home and abroad. 
Putin has attacked globalism and neo-liberalism and is identified by scholars with Russian conservatism. 
In cultural and social affairs Putin has collaborated closely with the Russian Orthodox Church. Patriarch Kirill of Moscow, head of the Church, endorsed his election in 2012 stating Putin's terms were like "a miracle of God."
"""


putin_testing_dialog = [
    # about Putin
    ("Tell me about Politicians", '''Welcome to politicians subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about terminology
Tell me about wars

Or move subtopic to specific politician (Putin, Xi Jinping):
Tell me about Putin

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Putin', 'vladimir vladimirovich putin'),
    ("who leads Russia?", "vladimir vladimirovich putin"),
    ('what is capital of Russia?', 'moscow'),
    ("who is russian president?", "vladimir vladimirovich putin"),
    ("who is russian president before Putin?", 'boris yeltsin'),
    ("who is Putin?", "a russian politician and former intelligence officer who is serving as the current president of russia"),  
    ("when did Putin born", "7 october 1952"),  
    ("Is Putin a KGB agent", "putin worked as a kgb foreign intelligence officer for 16 years , rising to the rank of lieutenant colonel"),
    ("does Putin support the church", "putin has collaborated closely with the russian orthodox church"),
    ("does russian church support Putin", 'putin has collaborated closely with the russian orthodox church . patriarch kirill of moscow , head of the church , endorsed his election in 2012 stating putin \' s terms were like " a miracle of god . "'),
    ("who lead russian orthodox church", "patriarch kirill of moscow"),
    ("what Kirill say about Putin", 'putin \' s terms were like " a miracle of god . "'),
    ("did putin wins election", 'putin received 76 % of the vote in the 2018 election and was re - elected for a six - year term ending in 2024 .'),
    ('Is Putin conservative?', 'putin has attacked globalism and neo - liberalism and is identified by scholars with russian conservatism'),
    ("is russia prosperous under putin", 'russian economy grew for eight straight years'),
    ("what did Putin do internationally", 'putin led russia to victory in the second chechen war and the russo - georgian war'),
    # tentang Putin
    ('siapa pemimpin Russia?', 'vladimir vladimirovich putin'),
    ('Apa ibukota Russia?', 'Moskwa'),
    ('siapa presiden Russia?', 'vladimir vladimirovich putin'),
    ('siapa presiden Russia sebelum Putin', 'boris yeltsin'),
    ('siapa itu Putin?', 'Seorang politisi Rusia dan mantan perwira intelijen yang menjabat sebagai presiden Rusia saat ini'),
    ('kapan Putin lahir?', '7 Oktober 1952'),
    ('Apakah Putin agen KGB?', 'putin bekerja sebagai perwira intelijen asing kgb selama 16 tahun'),
    ('Apakah gereja Russia mendukung Putin?', 'Putin telah bekerja sama erat dengan gereja ortodoks Rusia. Patriark Kirill dari Moskow, kepala gereja, mendukung pemilihannya pada tahun 2012 yang menyatakan bahwa istilah Putin seperti "keajaiban Tuhan."'), 
    ('Apakah Putin mendukung gereja?', 'Putin telah bekerja sama erat dengan gereja ortodoks Rusia'),
    ('Siapa pemimpin gereja Othodoks Russia?', 'patriark kirill dari Moskow'),
    ('apa pernyataan Kirill tentang Putin', 'Istilah Putin seperti "keajaiban Tuhan."'),
    ('Apakah Putin menang dalam pemilu?', 'Putin menerima 76% suara dalam pemilihan 2018 dan terpilih kembali untuk masa jabatan enam tahun yang berakhir pada 2024.'),
    ('apakah Putin konservatif?', 'Putin telah menyerang globalisme dan neo-liberalisme dan diidentifikasi oleh para sarjana dengan konservatisme Rusia.'),
    ('Apakah Russia membaik dibawah Putin?', 'Ekonomi Rusia tumbuh selama delapan tahun berturut-turut'),
    ('Apa yang Putin lakukan dalam dunia International', 'Putin memimpin Rusia menuju kemenangan dalam perang Chechnya kedua dan perang Rusia - Georgia'),
]