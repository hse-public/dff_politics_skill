egypt_wiki_text = """
The Arab Republic of Egypt is a transcontinental country spanning the northeast corner of Africa and the Sinai Peninsula of Western Asia. 

The president of Egypt is the executive head of state of Egypt. 
Under the various iterations of the Constitution of Egypt following the Egyptian Revolution of 1952, 
the president is also the supreme commander of the Armed Forces, 
and head of the executive branch of the Egyptian government. 
The current president of Egypt is Abdel Fattah el-Sisi, in office since 8 June 2014.

Abdel Fattah Saeed Hussein Khalil el-Sisi (born 19 November 1954) is a retired military officer and 
Egyptian politician who has served as the sixth and current president of Egypt since 2014.

The 2011 Egyptian revolution (color revolution / Arab Spring), also known as the 25 January revolution began on 25 January 2011 and spread across Egypt. 
Millions of protesters from a range of socio-economic and religious backgrounds demanded the overthrow of Egyptian President Hosni Mubarak.

After the revolution against Mubarak and a period of rule by the Supreme Council of the Armed Forces, 
the Muslim Brotherhood took power in Egypt through a series of popular elections, 
with Egyptians electing Islamist Mohamed Morsi to the presidency in June 2012.

On 3 July 2013, Morsi was deposed by a coup d'état led by the minister of defense, 
General Abdel Fattah El-Sisi, as millions of Egyptians took to the streets in support of early elections. 
El-Sisi went on to become Egypt's president after an election in 2014 which was boycotted by opposition parties.
"""

egypt_testing_dialog = [ 
    # in English
    ('Tell me about country', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('Tell me about Egypt', 'the arab republic of egypt is a transcontinental country spanning the northeast corner of africa and the sinai peninsula of western asia .'),
    ('what is the leader of Egypt called?','president'),
    ('when was Egypt revolution?','1952'),
    ('When was Egypt color revolution?','2011'),
    ('When was Arab Spring in Egypt? ','2011'),
    ('What happened during Egypt color revolution?','millions of protesters from a range of socio - economic and religious backgrounds demanded the overthrow of egyptian president hosni mubarak'),
    ('What happened during Egypt arab spring?','millions of protesters from a range of socio - economic and religious backgrounds demanded the overthrow of egyptian president hosni mubarak'),
    ('Which president was removed in color revolution?','hosni mubarak'),
    ('Who replace Hosni Mubarak as president?','mohamed morsi'),
    ('When Mohamed Morsi become president of Egypt?','june 2012'),
    ('When the last day Mohamed Morsi as president of Egypt?','3 july 2013'),
    ('How Mohamed Morsi was replaced?',"coup d ' etat"),
    ('Who lead coup against Morsi?','general abdel fattah el - sisi'),
    ('When was the coup against Morsi?','3 july 2013'),
    ('How Abdel Fattah el-sisi become president?','an election in 2014 which was boycotted by opposition parties'),
    ('Who is Abdel Fattah el-sisi?','current president of egypt'),
    ('Who is the current president of Egypt?','abdel fattah el - sisi'),
    ('When Abdel Fattah el-sisi become president?','8 june 2014'),
    # dalam bahasa Indonesia
    ('siapa pemimpin di Mesir?','Presiden Mesir'),
    ('Kapan revolusi Mesir terjadi?','1952'),
    ('kapan revolusi warna di Mesir?','2011'),
    ('Kapan musim semi Arab terjadi di Mesir? ','2011'),
    ('Apa yang terjadi di Mesir sewaktu revolusi warna?','Jutaan pengunjuk rasa dari berbagai latar belakang sosial - ekonomi dan agama menuntut penggulingan Presiden Mesir Hosni Mubarak'),
    ('Apa yang terjadi di Mesir sewaktu musim semi Arab?','Jutaan pengunjuk rasa dari berbagai latar belakang sosial - ekonomi dan agama menuntut penggulingan Presiden Mesir Hosni Mubarak'),
    ('Siapa presiden yg diturunkan oleh revolusi warna?','hosni mubarak'),
    ('Siapa yang menggantikan Hosni Mubarak?','mohamed morsi'),
    ('Kapan Mohamed Morsi menjadi presiden di Mesir?','Juni 2012'),
    ('Apakah Mohamed Morsi masih presiden di Mesir saat ini?','Presiden Mesir saat ini adalah Abdel Fattah el - sisi, yang menjabat sejak 8 Juni 2014. Abdel Fattah Saeed Hussein Khalil El - Sisi (lahir 19 November 1954) adalah seorang pensiunan perwira militer dan politisi Mesir yang telah menjabat sebagai presiden keenam dan saat ini mesir sejak 2014. Revolusi Mesir 2011 (revolusi warna/arab spring), juga dikenal sebagai revolusi 25 Januari dimulai pada 25 Januari 2011 dan menyebar ke seluruh Mesir. Jutaan pengunjuk rasa dari berbagai latar belakang sosial- ekonomi dan agama menuntut penggulingan Presiden Mesir Hosni Mubarak. Setelah revolusi melawan Mubarak dan periode pemerintahan oleh dewan tertinggi angkatan bersenjata, ikhwanul muslimin mengambil alih kekuasaan di Mesir melalui serangkaian pemilihan umum, dengan mesir memilih Islamis Mohamed Morsi menjadi presiden pada juni 2012.'),
    ('Siapa presiden Mesir sekarang?','abdel fattah el - sisi'),
    ('Kapan hari terakhir Mohamed Morsi sebagai presiden Mesir?','3 Juli 2013'),
    ('Bagaimana Mohamed Morsi digantikan?',"coup d ' etat"),
    ('Siapa melakukan kudeta terhadap Mohamed Morsi?','jenderal abdel fattah el - sisi'),
    ('Kapan terjadi kudeta terhadap Mohamed Morsi?','3 Juli 2013'),
    ('Bagaimana Abdel Fattah el-sisi menjadi presiden Mesir?','Pemilu 2014 yang diboikot oleh partai-partai oposisi'),
    ('Siapakah Abdel Fattah el-sisi?','Presiden Mesir saat ini'),
    ('Siapa presiden Mesir sekarang?','abdel fattah el - sisi'),
    ('Kapan Abdel Fattah el-sisi menjadi presiden?','2014'),
    ('Tanggal berapa Abdel Fattah el-sisi menjadi presiden?','8 Juni 2014'),
]