import os
from fastapi import responses 

import requests, uuid, json

# Add your subscription key and endpoint
subscription_key = os.getenv('AZURE_TRANSLATE_KEY')
endpoint = "https://api.cognitive.microsofttranslator.com"

# Add your location, also known as region. The default is global.
# This is required if using a Cognitive Services resource.
location = "southeastasia"

path = '/translate'
constructed_url = endpoint + path

params = {
    'api-version': '3.0',
    'from': 'id',
    'to': 'en'
}
constructed_url = endpoint + path

headers = {
    'Ocp-Apim-Subscription-Key': subscription_key,
    'Ocp-Apim-Subscription-Region': location,
    'Content-type': 'application/json',
    'X-ClientTraceId': str(uuid.uuid4())
}

def azure_detect_lang(text):
    """Detect the source language via Azure Translate API"""
    path = '/detect'
    constructed_url = endpoint + path 
    params = {
        'api-version': '3.0'
    }

    body = [{
        'text': text
    }]

    response = requests.post(constructed_url, params=params, headers=headers, json=body)
    response = response.json()[0]
    return response['language'] 

def azure_translate(text, fr, to):
    """Call Azure Translate API"""
    
    params['from'] = fr
    params['to'] = to
    
    body = [{
        'text': text
    }]

    response = requests.post(constructed_url, params=params, headers=headers, json=body)
    response = response.json()[0]
    return response['translations'][0]['text']

def azure_translate_id_to_en(text):
    return azure_translate(text, 'id', 'en')

def azure_translate_en_to_id(text):
    return azure_translate(text, 'en', 'id')

if __name__ == '__main__':
    text = 'apa Xi bergabung dengan PKT?'
    en_text = azure_translate_id_to_en(text)
    id_text = azure_translate_en_to_id(en_text)
    print(f"Original text: {text}")
    print(f"English text: {en_text}")
    print(f"Indonesian text: {id_text}")