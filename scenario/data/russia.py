russia_wiki_text = """
Russia, or the Russian Federation, is a country spanning Eastern Europe and Northern Asia. 
Russia is the largest country in the world, covering over 17,125,191 square kilometres (6,612,073 sq mi), and encompassing one-eighth of Earth's inhabitable landmass. 
Russia extends across eleven time zones, and has the most borders of any country in the world, with sixteen sovereign nations. 
Russia has a population of 146.2 million; and is the most populous country in Europe, 
and the ninth-most populous country in the world. 
Moscow, the capital, is the largest city entirely within Europe; 
while Saint Petersburg is the country's second-largest city and cultural centre.

By the 18th century, Russia had vastly expanded through conquest, annexation, and exploration to evolve into the Russian Empire, the third-largest empire in history. 
Following the Russian Revolution, the Russian SFSR became the largest and leading constituent of the Soviet Union, the world's first constitutionally socialist state. 
The Soviet Union played a decisive role in the Allied victory in World War II, 
and emerged as a superpower and rival to the United States during the Cold War. 

The Soviet era saw some of the most significant technological achievements of the 20th century, 
including the world's first human-made satellite and the launching of the first human in space.

Following the dissolution of the Soviet Union in 1991, the Russian SFSR reconstituted itself as the Russian Federation. 
In the aftermath of the constitutional crisis of 1993, 
a new constitution was adopted, and Russia has since been governed as a federal semi-presidential republic. 
"""

russia_testing_dialog = [
        ('Tell me about nations', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
        ('Tell me about russia', "russia , or the russian federation , is a country spanning eastern europe and northern asia . russia is the largest country in the world , covering over 17 , 125 , 191 square kilometres ( 6 , 612 , 073 sq mi ) , and encompassing one - eighth of earth ' s inhabitable landmass . russia extends across eleven time zones , and has the most borders of any country in the world , with sixteen sovereign nations . russia has a population of 146 . 2 million ; and is the most populous country in europe , and the ninth - most populous country in the world . moscow , the capital , is the largest city entirely within europe ; while saint petersburg is the country ' s second - largest city and cultural centre . by the 18th century , russia had vastly expanded through conquest , annexation , and exploration to evolve into the russian empire , the third - largest empire in history . following the russian revolution , the russian sfsr became the largest and leading constituent of the soviet union , the world ' s first constitutionally socialist state . the soviet union played a decisive role in the allied victory in world war ii , and emerged as a superpower and rival to the united states during the cold war . the soviet era saw some of the most significant technological achievements of the 20th century , including the world ' s first human - made satellite and the launching of the first human in space . following the dissolution of the soviet union in 1991 , the russian sfsr reconstituted itself as the russian federation . in the aftermath of the constitutional crisis of 1993 , a new constitution was adopted , and russia has since been governed as a federal semi - presidential republic ."),
        ('What is Russia', 'russia , or the russian federation , is a country spanning eastern europe and northern asia'),
        ('How big is Russia', '17 , 125 , 191 square kilometres ( 6 , 612 , 073 sq mi )'),
        ('How many timezone in Russia', 'eleven'),
        ('Did Russia involved in world war', 'the soviet union played a decisive role in the allied victory in world war ii'),
        ('What happened after Russia revolution', 'the russian sfsr became the largest and leading constituent of the soviet union'),
        ('What happened in Soviet era', "some of the most significant technological achievements of the 20th century , including the world ' s first human - made satellite and the launching of the first human in space"),
        ('Who rules Russia today', 'federal semi - presidential republic'),
        ('How is Russia govern today', 'as a federal semi - presidential republic'),
        # dalam Bahasa Indonesia
        ('Ceritakan tentang Russia', 'Rusia, atau Federasi Rusia, adalah negara yang mencakup Eropa Timur dan Asia Utara. Rusia adalah negara terbesar di dunia.'),
        ('Apa itu Russia', 'Rusia, atau Federasi Rusia, adalah negara yang mencakup Eropa Timur dan Asia Utara.'),
        ('Berapa besar Russia', '17, 125 , 191 kilometer persegi (6,612 , 073 sq mi)'),
        ('Berapa zona waktu di Russia', 'Rusia meluas di sebelas zona waktu'),
        ('Apa yang terjadi setelah revolusi di Russia', 'Sfsr Rusia menjadi konstituen terbesar dan terkemuka dari Uni Soviet.'),
        ('Apakah Russia terlibat perang dunia', 'Perang Dunia II'),
        ('Apa yang terjadi zaman Uni Soviet', 'Pembubaran Uni Soviet'),
        ('Apa kehebatan Uni Soviet', 'Uni Soviet memainkan peran yang menentukan dalam kemenangan sekutu dalam Perang Dunia II, dan muncul sebagai negara adidaya dan saingan amerika serikat selama perang dingin. Era Soviet melihat beberapa pencapaian teknologi paling signifikan di abad ke-20.'),
        ('Siapa memerintah Russia', 'semi federal - republik presidensial'),
]