#!/usr/bin/env python

import time
import re
from typing import Dict, Optional, Union, Tuple

from scenario.main import actor
from df_engine.core import Actor, Context
from annotators.main import annotate

from scenario.utils.translation_service import translate_id_to_en, translate_en_to_id, detect_lang

# some constants
idontknow = "Sorry, I don't know about that."
idontknow_id = "Maaf, saya tidak tahu tentang itu."
qmark_pat = re.compile(r'\?$')

def normalize_request(request: str) -> Tuple[str, bool]:
    not_english = False
    id_has_qmark = False
    if detect_lang(request) != 'en':
        not_english = True
        request = request.strip()
        id_has_qmark = request.endswith('?')
        request = translate_id_to_en(request)
        print(f"translated to: {request}")
        # remove question mark if original Indonesia didn't have one
        if not id_has_qmark:
            request = re.sub(qmark_pat, '', request)
        # print(f"translated text: {request}")
    return request, not_english

def post_process_answer(answer: str, not_english: bool) -> str: 
    if not re.search(r'\w+',answer):
        return idontknow_id if not_english else idontknow
    if not_english:
        return translate_en_to_id(answer)
    else:
        return answer

def turn_handler(
    in_request: str,
    ctx: Union[Context, str, dict],
    actor: Actor,
    true_out_response: Optional[str] = None,
):
    # Context.cast - gets an object type of [Context, str, dict] returns an object type of Context
    ctx = Context.cast(ctx)

    # Add in current context a next request of user
    in_request, not_english = normalize_request(in_request)
    ctx.add_request(in_request)
    ctx = annotate(ctx)

    # pass the context into actor and it returns updated context with actor response
    ctx = actor(ctx)
    # get last actor response from the context
    out_response = post_process_answer(ctx.last_response, not_english)
    # the next condition branching needs for testing
    if true_out_response is not None and true_out_response != out_response:
        raise Exception(f"{in_request=} -> true_out_response != out_response: {true_out_response} != {out_response}")
    return out_response, ctx


if __name__ == "__main__":
    ctx = {}
    while True:
        in_request = input("type your request: ")
        st_time = time.time()
        out_response, ctx = turn_handler(in_request, ctx, actor)
        print(f"\nQuestion: {in_request}")
        print(f"Answer: {out_response}")
        total_time = time.time() - st_time
        print(f"exec time = {total_time:.3f}s")