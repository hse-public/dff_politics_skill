from google.cloud import translate_v2 as translate 

translate_client = translate.Client()

def google_translate_to_en(text):
    return translate_client.translate(text, target_language='en')['translatedText']

def google_translate_to_id(text):
    return translate_client.translate(text, target_language='id')['translatedText']

def google_detect_lang(text):
    return translate_client.detect_language(text)['language']