republics_wiki_text = """
A republic (from Latin res publica 'public affair') is a form of government in which "supreme power is held by the people and their elected representatives". 
In republics, the country is considered a "public matter", not the private concern or property of the rulers. 
The primary positions of power within a republic are attained through democracy or a mix of democracy with oligarchy or autocracy rather than being unalterably occupied by any given family lineage or group. 
With modern republicanism, it has become the opposing form of government to a monarchy and therefore a modern republic has no monarch as head of state.

As of 2017, 159 of the world's 206 sovereign states use the word "republic" as part of their official names. 
Not all of these are republics in the sense of having elected governments, nor is the word "republic" used in the names of all states with elected governments.

The word republic comes from the Latin term res publica, which literally means "public thing", "public matter", or "public affair" and was used to refer to the state as a whole. 
The term developed its modern meaning in reference to the constitution of the ancient Roman Republic, lasting from the overthrow of the kings in 509 BC to the establishment of the Empire in 27 BC. 
This constitution was characterized by a Senate composed of wealthy aristocrats wielding significant influence; 
several popular assemblies of all free citizens, possessing the power to elect magistrates and pass laws; 
and a series of magistracies with varying types of civil and political authority.

Most often a republic is a single sovereign state, but there are also sub-sovereign state entities that are referred to as republics, 
or that have governments that are described as republican in nature.
"""

republics_testing_dialog = [
    ('Tell me about terminologies', '''Welcome to terminologies subtopics. 
You could move to other subtopics with:
Tell me about countries
Tell me about wars

Or move subtopic to specific terminologies (politics, monarchy, republics, democracy):
Tell me about republics

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about republic', '''a republic ( from latin res publica ' public affair ' ) is a form of government in which " supreme power is held by the people and their elected representatives "'''),
    ('how is republic originated', 'the word republic comes from the latin term res publica'),
    ('how many countries are republics', "159 of the world ' s 206"),
    ('what is opposite of republics', 'a monarchy'),
    ('ceritakan tentang republic', 'kekuasaan tertinggi dipegang oleh rakyat dan wakil-wakil pilihan mereka."'),
    ('bagaimana asal mula republic', 'latin term res publica'),
    ('berapa negara berbentuk republic', '159 dari dunia 206'),
    ('apa lawan dari republics', 'Monarki'),
]