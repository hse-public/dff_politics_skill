# import model and tokenizer 
from re import I
from transformers import BertForQuestionAnswering, cached_path
from transformers import BertTokenizer
# Torch 
import torch 

from scenario.data.putin import putin_wiki_text
from scenario.data.xi_jinping import xi_wiki_text
from scenario.data.china import china_wiki_text
from scenario.data.russia import russia_wiki_text
from scenario.data.usa import usa_wiki_text
from scenario.data.indonesia import indonesia_wiki_text
from scenario.data.libya import libya_wiki_text
from scenario.data.iran import iran_wiki_text
from scenario.data.syria import syria_wiki_text
from scenario.data.india import india_wiki_text
from scenario.data.germany import germany_wiki_text
from scenario.data.france import france_wiki_text
from scenario.data.egypt import egypt_wiki_text
from scenario.data.world_war_1 import world_war_1_wiki_text
from scenario.data.world_war_2 import world_war_2_wiki_text
from scenario.data.cold_war import cold_war_wiki_text
from scenario.data.politics import politics_wiki_text
from scenario.data.monarchy import monarchy_wiki_text
from scenario.data.democracy import democracy_wiki_text
from scenario.data.republics import republics_wiki_text

# loading the pre trained Model
qna_model = BertForQuestionAnswering.from_pretrained('bert-large-uncased-whole-word-masking-finetuned-squad', cache_dir="huggingface_cache")

# loading the Tokenizer from the same model
qna_tokenizer = BertTokenizer.from_pretrained('bert-large-uncased-whole-word-masking-finetuned-squad', cache_dir="huggingface_cache")

# function to get an answer for a use given question
def QnA(user_input_que, wiki_text):
    # tokenizing the text 
    in_tok = qna_tokenizer.encode_plus(user_input_que, wiki_text,
                                       return_tensors='pt')
    # getting scores from tokens
    ans_str_sc, ans_en_sc = qna_model(**in_tok, return_dict=False)
    
    # getting the position
    ans_st = torch.argmax(ans_str_sc)
    ans_en = torch.argmax(ans_en_sc) + 1

    # ids are then converted to tokens
    ans_tok = qna_tokenizer.convert_ids_to_tokens(in_tok["input_ids"][0][ans_st:ans_en])

    # getting the answer 
    return qna_tokenizer.convert_tokens_to_string(ans_tok)


def QnA_Putin(user_input):
    return QnA(user_input, putin_wiki_text)

def QnA_Xi(user_input):
    return QnA(user_input, xi_wiki_text)

def QnA_China(user_input):
    return QnA(user_input, china_wiki_text)

def QnA_Russia(user_input):
    return QnA(user_input, russia_wiki_text)

def QnA_USA(user_input):
    return QnA(user_input, usa_wiki_text)

def QnA_Indonesia(user_input):
    return QnA(user_input, indonesia_wiki_text)

def QnA_Libya(user_input):
    return QnA(user_input, libya_wiki_text)

def QnA_Syria(user_input):
    return QnA(user_input, syria_wiki_text)

def QnA_Egypt(user_input):
    return QnA(user_input, egypt_wiki_text)

def QnA_Iran(user_input):
    return QnA(user_input, iran_wiki_text)

def QnA_Germany(user_input):
    return QnA(user_input, germany_wiki_text)

def QnA_France(user_input):
    return QnA(user_input, france_wiki_text)

def QnA_India(user_input):
    return QnA(user_input, india_wiki_text)

def QnA_WW1(user_input):
    return QnA(user_input, world_war_1_wiki_text)

def QnA_WW2(user_input):
    return QnA(user_input, world_war_2_wiki_text)

def QnA_ColdWar(user_input):
    return QnA(user_input, cold_war_wiki_text)

def QnA_Politics(user_input):
    return QnA(user_input, politics_wiki_text)

def QnA_Monarchy(user_input):
    return QnA(user_input, monarchy_wiki_text)

def QnA_Republics(user_input):
    return QnA(user_input, republics_wiki_text)

def QnA_Democracy(user_input):
    return QnA(user_input, democracy_wiki_text)