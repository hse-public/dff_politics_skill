china_wiki_text = """
China (Chinese: 中国; pinyin: Zhōngguó), officially the People's Republic of China (PRC; Chinese: 中华人民共和国; pinyin: Zhōnghuá Rénmín Gònghéguó), is a country in East Asia. 
China is the world's most populous country, with a population of more than 1.4 billion.

China consists of 23 provinces, four municipalities, five autonomous regions, and two Special Administrative Regions (Hong Kong and Macau). 
The national capital is Beijing.

In the 3rd century BCE, the Qin reunited core China and established the first Chinese empire. 
Han dynasty (206 BCE – 220 CE) which succeed Qin saw some of the most advanced technology at that time, including papermaking and the compass, along with agricultural and medical improvements. 
The invention of gunpowder and movable type in the Tang dynasty (618–907) and Northern Song dynasty (960–1127) completed the Four Great Inventions. 
Tang culture spread widely in Asia, as the new Silk Road brought traders to as far as Mesopotamia and the Horn of Africa. 
The Qing dynasty, China's last dynasty, which formed the territorial basis for modern China, suffered heavy losses to foreign imperialism in the 19th century.

The Chinese monarchy collapsed in 1912 with the Xinhai Revolution, when the Republic of China (ROC) replaced the Qing dynasty. 
China was invaded by the Empire of Japan during World War II. The Civil War resulted in a division of territory in 1949 when the Communist Party (CCP) established the People's Republic of China (PRC) on the mainland while the Kuomintang-led ROC government retreated to the island of Taiwan. 
Both claim to be the sole legitimate government of China, although the United Nations has recognized the PRC as the sole representation since 1971.
"""


china_testing_dialog = [
    # about China
    ('Tell me about nations', '''Welcome to countries subtopics. 
You could move to other subtopics with:
Tell me about terminologies
Tell me about wars

Or move subtopic to specific country (china, egypt, france, germany, india, indonesia,
iran, libya, russia, syria, usa):
Tell me about China    

Or start from beginning: 
Please restart
Start from beginning
'''),
    ('tell me about china', "china is the world ' s most populous country"),
    ('what is china', "china ( chinese : 中 国 ; pinyin : zhongguo ) , officially the people ' s republic of china ( prc ; chinese : 中 [UNK] 人 民 [UNK] 和 国 ; pinyin : zhonghua renmin gongheguo ) , is a country in east asia"),
    ('what was first empire in China', 'in the 3rd century bce , the qin reunited core china and established the first chinese empire'),
    ('who establish first china',  'the qin reunited core china and established the first chinese empire'),
    ('what did qin empire do',  'established the first chinese empire'),
    ('who replace qin',  'han dynasty'),
    ('What did han dynasty do', 'saw some of the most advanced technology at that time , including papermaking and the compass , along with agricultural and medical improvements'),
    ('what were great inventions in China',  'the invention of gunpowder and movable type in the tang dynasty ( 618 – 907 ) and northern song dynasty ( 960 – 1127 ) completed the four great inventions'),
    ('what was last dynasty in China',  'the qing dynasty'),
    ('who replace qing dynasty',  'republic of china ( roc )'),
    ('who rules china today',  "people ' s republic of china"),
    # dalam Bahasa Indonesia
    ('Siapa kerajaan pertama','Qin bersatu kembali inti Cina dan mendirikan kekaisaran Cina pertama'),
    ('Apa yang Qin lakukan','menyatukan kembali inti Cina dan mendirikan kekaisaran Cina pertama'),
    ('Siapa menggantikan Qin','Dinasti Han'),
    ('Apa yang dilakukan dinasti Han','Melihat beberapa teknologi paling canggih pada waktu itu, termasuk pembuatan kertas dan kompas, bersama dengan perbaikan pertanian dan medis.'),
    ('Apa penemuan terbesar di China','penemuan bubuk mesiu dan tipe bergerak'),
    ('Apa karya terbesar di China','Penemuan bubuk mesiu dan jenis bergerak dalam dinasti tang (618-907) dan dinasti lagu utara (960-1127) menyelesaikan empat penemuan besar.'),
    ('Apa dinasty terakhir di China','Dinasti Qing'),
    ('Siapa menggantikan dinasty Qing','Republik Cina'),
    ('Siapa berkuasa di China sekarang','Republik rakyat Cina'),
    ('Siapa pemerintahan China sekarang','Republik rakyat Cina'),
]