from scenario.utils.azure_translate import azure_translate_en_to_id, \
            azure_translate_id_to_en, azure_detect_lang
# from scenario.utils.google_translate import google_translate_to_en, \
#             google_translate_to_id, google_detect_lang

# choose translation service
# options: azure or google
# note: the current integration-test scenarios for Indonesian 
#        will only work with azure  
service = 'azure' 

if service == 'azure':
    translate_id_to_en = azure_translate_id_to_en
    translate_en_to_id = azure_translate_en_to_id
    detect_lang = azure_detect_lang
# elif service == 'google':
#     translate_id_to_en = google_translate_to_en
#     translate_en_to_id = google_translate_to_id
#     detect_lang = google_detect_lang
else:
    raise RuntimeError("Translation service not available")